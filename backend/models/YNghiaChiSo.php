<?php
namespace backend\models;
use Yii;
/**
 * This is the model class for table "{{%y_nghia_chi_so}}".
 *
 * @property int $id
 * @property int|null $y_nghia_id
 * @property int|null $chi_so_id
 *
 * @property ChiSo $chiSo
 * @property YNghiaCacConSo $yNghia
 */
class YNghiaChiSo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%y_nghia_chi_so}}';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['y_nghia_id', 'chi_so_id'], 'integer'],
            [['chi_so_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChiSo::className(), 'targetAttribute' => ['chi_so_id' => 'id']],
            [['y_nghia_id'], 'exist', 'skipOnError' => true, 'targetClass' => YNghiaCacConSo::className(), 'targetAttribute' => ['y_nghia_id' => 'id']],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'y_nghia_id' => 'Y Nghia ID',
            'chi_so_id' => 'Chi So ID',
        ];
    }
    /**
     * Gets query for [[ChiSo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiSo()
    {
        return $this->hasOne(ChiSo::className(), ['id' => 'chi_so_id']);
    }
    /**
     * Gets query for [[YNghia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getYNghia()
    {
        return $this->hasOne(YNghiaCacConSo::className(), ['id' => 'y_nghia_id']);
    }
}
