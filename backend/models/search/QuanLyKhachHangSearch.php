<?php

namespace backend\models\search;

use backend\models\QuanLyKhachHang;
use backend\models\QuanLyUser;
use backend\models\Vaitrouser;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Cauhinh;

/**
 * CauhinhSearch represents the model behind the search form about `backend\models\Cauhinh`.
 */
class QuanLyKhachHangSearch extends QuanLyKhachHang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'VIP', 'hoat_dong', 'kinh_doanh_id', 'created_at_tu'], 'safe'],
            [['nhom'], 'safe'],
            [['vi_dien_tu'], 'safe'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'anhdaidien', 'kinh_doanh'], 'safe'],
            [['password_reset_token'], 'safe'],
            [['auth_key'], 'safe'],
            [['dien_thoai', 'cmnd'], 'safe'],
            [['dia_chi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuanLyKhachHang::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->created_at != '')
            $query->andFilterWhere(['<=', 'date(created_at)', myAPI::convertDateSaveIntoDb($this->created_at)]);
        if($this->created_at_tu != '')
            $query->andFilterWhere(['>=', 'date(created_at)', myAPI::convertDateSaveIntoDb($this->created_at_tu)]);


        if(!User::isViewAll())
            $query->andFilterWhere(['dai_ly_id' => Yii::$app->user->id]);

        $query->andFilterWhere(['like', 'hoten', $this->hoten])
            ->andFilterWhere(['like', 'dien_thoai', $this->dien_thoai])
            ->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
