<?php

namespace backend\models\search;

use backend\models\QuanLyGiaoDich;
use common\models\myAPI;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Cauhinh;

/**
 * CauhinhSearch represents the model behind the search form about `backend\models\Cauhinh`.
 */
class QuanLyGiaoDichSearch extends QuanLyGiaoDich
{
    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['id', 'khach_hang_id', 'don_hang_lien_quan_id', 'user_id'], 'safe'],
            [['trang_thai_giao_dich', 'loai_giao_dich', 'trang_thai'], 'safe'],
            [['so_tien_giao_dich', 'created_to', 'so_tien_giao_dich_to'], 'safe'],
            [['created', 'ngay_cap_nhat'], 'safe'],
            [['ma_giao_dich', 'ma_giao_dich_khach_paste', 'nguoi_cap_nhat', 'nguoi_tao', 'khach_hang'], 'safe'],
            [['ghi_chu'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuanLyGiaoDich::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['active' => 1]);

        if($this->created !='')
            $query->andFilterWhere(['>=', 'date(created)', myAPI::convertDateSaveIntoDb($this->created)]);
        if($this->created_to !='')
            $query->andFilterWhere(['<=', 'date(created)', myAPI::convertDateSaveIntoDb($this->created_to)]);

        if($this->so_tien_giao_dich !='')
            $query->andFilterWhere(['>=', 'so_tien_giao_dich', $this->so_tien_giao_dich]);
        if($this->so_tien_giao_dich_to !='')
            $query->andFilterWhere(['<=', 'so_tien_giao_dich_to', $this->so_tien_giao_dich_to]);

        $query
            ->andFilterWhere(['like', 'don_hang_lien_quan_id', $this->don_hang_lien_quan_id])
            ->andFilterWhere(['like', 'nguoi_tao', $this->nguoi_tao])
            ->andFilterWhere(['like', 'khach_hang', $this->khach_hang]);

        $query->andFilterWhere([
            'id' => $this->id,
            'trang_thai' => $this->trang_thai,
            'loai_giao_dich' => $this->loai_giao_dich
        ]);

        return $dataProvider;
    }

    public function searchMyTransaction($params)
    {
        $query = QuanLyGiaoDich::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);
        $query->andFilterWhere(['khach_hang_id' => Yii::$app->user->id]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['active' => 1]);

        if($this->created !='')
            $query->andFilterWhere(['>=', 'date(created)', myAPI::convertDateSaveIntoDb($this->created)]);
        if($this->created_to !='')
            $query->andFilterWhere(['<=', 'date(created)', myAPI::convertDateSaveIntoDb($this->created_to)]);

        if($this->so_tien_giao_dich !='')
            $query->andFilterWhere(['>=', 'so_tien_giao_dich', $this->so_tien_giao_dich]);
        if($this->so_tien_giao_dich_to !='')
            $query->andFilterWhere(['<=', 'so_tien_giao_dich_to', $this->so_tien_giao_dich_to]);

        $query
            ->andFilterWhere(['like', 'don_hang_lien_quan_id', $this->don_hang_lien_quan_id])
            ->andFilterWhere(['like', 'nguoi_tao', $this->nguoi_tao])
            ->andFilterWhere(['like', 'khach_hang', $this->khach_hang]);

        $query->andFilterWhere([
            'id' => $this->id,
            'trang_thai' => $this->trang_thai,
            'loai_giao_dich' => $this->loai_giao_dich
        ]);

        return $dataProvider;
    }
}
