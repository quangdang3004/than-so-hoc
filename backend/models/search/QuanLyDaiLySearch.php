<?php

namespace backend\models\search;

use app\models\QuanLyDaiLy;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Cauhinh;

/**
 * CauhinhSearch represents the model behind the search form about `backend\models\Cauhinh`.
 */
class QuanLyDaiLySearch extends QuanLyDaiLy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'VIP', 'dai_ly_id', 'so_lan_xem_con_lai', 'so_luong_khach'], 'safe'],
            [['vi_dien_tu'], 'safe'],
            [['ngay_sinh'], 'safe'],
            [['thong_tin_chi_so'], 'safe'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'anhdaidien', 'code_ho_ten'], 'safe'],
            [['password_reset_token'], 'safe'],
            [['auth_key'], 'safe'],
            [['dien_thoai', 'cmnd'], 'safe'],
            [['dia_chi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuanLyDaiLy::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        return $dataProvider;
    }
}
