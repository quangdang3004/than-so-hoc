<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "rv_quan_ly_khach_hang".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $password
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $dia_chi
 * @property string|null $cmnd
 * @property string|null $khu_vuc
 * @property string|null $anhdaidien
 * @property int|null $VIP
 * @property float|null $vi_dien_tu
 * @property int|null $dai_ly_id
 * @property string|null $ngay_sinh
 * @property string|null $code_ho_ten
 * @property string|null $dai_ly
 */
class QuanLyKhachHang extends \yii\db\ActiveRecord
{
    public $created_at_tu;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rv_quan_ly_khach_hang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'VIP', 'dai_ly_id'], 'integer'],
            [['vi_dien_tu'], 'number'],
            [['ngay_sinh', 'khu_vuc'], 'safe'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'anhdaidien', 'code_ho_ten', 'dai_ly'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'hoten' => 'Hoten',
            'dien_thoai' => 'Dien Thoai',
            'dia_chi' => 'Dia Chi',
            'cmnd' => 'Cmnd',
            'anhdaidien' => 'Anhdaidien',
            'VIP' => 'Vip',
            'vi_dien_tu' => 'Vi Dien Tu',
            'dai_ly_id' => 'Dai Ly ID',
            'ngay_sinh' => 'Ngay Sinh',
            'code_ho_ten' => 'Code Ho Ten',
            'dai_ly' => 'Dai Ly',
        ];
    }
}
