<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table rv_images".
 *
 * @property int $id
 * @property string $link_image
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rv_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'link_image'], 'required'],
            [['id'], 'integer'],
            [['link_image'], 'string', 'max' => 300],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_image' => 'Link Image',
        ];
    }
}
