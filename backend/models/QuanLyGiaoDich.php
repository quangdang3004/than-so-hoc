<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_giao_dich}}".
 *
 * @property int $id
 * @property int|null $khach_hang_id
 * @property string|null $ma_giao_dich
 * @property string|null $trang_thai_giao_dich
 * @property string|null $loai_giao_dich
 * @property int|null $don_hang_lien_quan_id
 * @property float|null $so_tien_giao_dich
 * @property string|null $ghi_chu
 * @property string|null $ma_giao_dich_khach_paste
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $so_luot_xem
 * @property int|null $giao_dich_nap_tien_id
 * @property int|null $active
 * @property string|null $ngay_giao_dich
 * @property float|null $khuyen_mai
 * @property int|null $so_bai_con_lai
 * @property int|null $mua_goi
 * @property float|null $don_gia
 * @property float|null $so_tien
 * @property int|null $cho_phep_thong_ke
 * @property string|null $trang_thai
 * @property string|null $nguoi_cap_nhat
 * @property string|null $nguoi_tao
 * @property string|null $ngay_cap_nhat
 * @property string|null $khach_hang
 */

class QuanLyGiaoDich extends \yii\db\ActiveRecord
{
    public $created_to;
    public $so_tien_giao_dich_to;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%quan_ly_giao_dich}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'khach_hang_id', 'don_hang_lien_quan_id', 'user_id', 'so_luot_xem', 'giao_dich_nap_tien_id', 'active', 'so_bai_con_lai', 'mua_goi', 'cho_phep_thong_ke'], 'integer'],
            [['trang_thai_giao_dich', 'loai_giao_dich', 'trang_thai'], 'string'],
            [['so_tien_giao_dich', 'khuyen_mai', 'don_gia', 'so_tien'], 'number'],
            [['created', 'ngay_giao_dich', 'ngay_cap_nhat'], 'safe'],
            [['ma_giao_dich', 'ma_giao_dich_khach_paste', 'nguoi_cap_nhat', 'nguoi_tao', 'khach_hang'], 'string', 'max' => 100],
            [['ghi_chu'], 'string', 'max' => 300],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'khach_hang_id' => 'Khach Hang ID',
            'ma_giao_dich' => 'Ma Giao Dich',
            'trang_thai_giao_dich' => 'Trang Thai Giao Dich',
            'loai_giao_dich' => 'Loai Giao Dich',
            'don_hang_lien_quan_id' => 'Don Hang Lien Quan ID',
            'so_tien_giao_dich' => 'So Tien Giao Dich',
            'ghi_chu' => 'Ghi Chú',
            'ma_giao_dich_khach_paste' => 'Ma Giao Dich Khach Paste',
            'trang_thai' => 'Trang Thai',
            'created' => 'Created',
            'user_id' => 'User ID',
        ];
    }
}
