<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "rv_voucher".
 *
 * @property int $id
 * @property string|null $ma
 * @property string|null $ngay_su_dung
 *
 * @property User[] $users
 */
class Voucher extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rv_voucher';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ngay_su_dung'], 'safe'],
            [['ma'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ma' => 'Ma',
            'ngay_su_dung' => 'Ngay Su Dung',
        ];
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['voucher_id' => 'id']);
    }
}
