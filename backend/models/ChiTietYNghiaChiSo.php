<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%chi_tiet_y_nghia_chi_so}}".
 *
 * @property int $id
 * @property int|null $y_nghia_id
 * @property int|null $chi_so_id
 * @property float|null $gia_tri
 * @property string|null $ghi_chu
 * @property string|null $noi_dung
 * @property int|null $khach_hang_id
 * @property int|null $so_hai_con_so
 * @property string|null $loai_chi_so
 * @property string|null $noi_dung_con_so
 */
class ChiTietYNghiaChiSo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%chi_tiet_y_nghia_chi_so}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'y_nghia_id', 'chi_so_id', 'khach_hang_id', 'so_hai_con_so'], 'integer'],
            [['gia_tri'], 'number'],
            [['noi_dung', 'loai_chi_so', 'noi_dung_con_so'], 'string'],
            [['ghi_chu'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'y_nghia_id' => 'Y Nghia ID',
            'chi_so_id' => 'Chi So ID',
            'gia_tri' => 'Gia Tri',
            'ghi_chu' => 'Ghi Chu',
            'noi_dung' => 'Noi Dung',
            'khach_hang_id' => 'Khach Hang ID',
            'so_hai_con_so' => 'So Hai Con So',
            'loai_chi_so' => 'Loai Chi So',
            'noi_dung_con_so' => 'Noi Dung Con So',
        ];
    }
}
