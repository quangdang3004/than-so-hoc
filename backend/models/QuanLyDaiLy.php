<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_dai_ly}}".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $password
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $dia_chi
 * @property string|null $cmnd
 * @property string|null $anhdaidien
 * @property int|null $VIP
 * @property float|null $vi_dien_tu
 * @property int|null $dai_ly_id
 * @property string|null $ngay_sinh
 * @property string|null $code_ho_ten
 * @property int|null $so_lan_xem_con_lai
 * @property string|null $thong_tin_chi_so
 * @property string|null $gio_sinh
 * @property string|null $gio_sinh_str
 * @property string|null $nguon_tra_cuu
 * @property int|null $khu_vuc_id
 * @property string|null $role_user
 * @property float|null $so_tien_can_thanh_toan
 * @property float|null $so_tien_da_thanh_toan
 * @property float|null $con_no
 * @property string|null $ma_dai_ly
 * @property float|null $doanh_thu
 * @property float|null $da_chi
 * @property float|null $so_tien
 * @property float|null $khuyen_mai
 * @property int|null $goi_id
 * @property float|null $loi_nhuan
 * @property string|null $ngay_thanh_toan_lan_dau
 * @property float|null $loi_nhuan_dai_ly
 * @property int $so_luong_khach
 * @property int $so_luong_dai_ly
 * @property string|null $dai_ly
 * @property string|null $khu_vuc
 */

class QuanLyDaiLy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rv_quan_ly_dai_ly';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'VIP', 'dai_ly_id', 'so_lan_xem_con_lai', 'so_luong_khach'], 'integer'],
            [['vi_dien_tu'], 'number'],
            [['ngay_sinh'], 'safe'],
            [['thong_tin_chi_so'], 'string'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'anhdaidien', 'code_ho_ten'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'hoten' => 'Hoten',
            'dien_thoai' => 'Dien Thoai',
            'dia_chi' => 'Dia Chi',
            'cmnd' => 'Cmnd',
            'anhdaidien' => 'Anhdaidien',
            'VIP' => 'Vip',
            'vi_dien_tu' => 'Vi Dien Tu',
            'dai_ly_id' => 'Dai Ly ID',
            'ngay_sinh' => 'Ngay Sinh',
            'code_ho_ten' => 'Code Ho Ten',
            'so_lan_xem_con_lai' => 'So Lan Xem Con Lai',
            'thong_tin_chi_so' => 'Thong Tin Chi So',
            'so_luong_khach' => 'So Luong Khach',
        ];
    }
}
