<?php
/**
 * Created by PhpStorm.
 * User: ibox
 * Date: 2019-03-07
 * Time: 00:16
 */

namespace backend\controllers;

use backend\models\Cauhinh;
use backend\models\GiaoDich;
use backend\models\QuanLyGiaoDich;
use backend\models\TrangThaiGiaoDich;
use common\models\myAPI;
use common\models\RanVangAPI;
use common\models\ThanSoHoc;
use common\models\User;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\YiiAsset;

class GiaoDichController extends CoreApiController
{
    public function behaviors()
    {

        $arr_action = ['nhan-ma-giao-dich', 'dai-ly-nap-tien', 'get-data'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('GiaoDich', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    /**
     * @inheritdoc /nhan-ma-giao-dich
     */
    public function actionNhanMaGiaoDich(){
        $random =  rand(10000, 99999);
//        $cauHinhSMTP = CauHinhSMTP::findOne(1);
        $giaodich = new GiaoDich();

        if(isset($this->dataPost['phan_loai'])){ // Khách hàng thanh toán tiền
            $random.= $this->dataPost['khach_hang']['id'];
            $giaodich->khach_hang_id = $this->dataPost['khach_hang']['id'];
            $khachHang = User::findOne($giaodich->khach_hang_id);
            $giaodich->so_tien_giao_dich = intval(str_replace(',','', $this->dataPost['so_tien_giao_dich']));

            $daiLyHienTai = User::findOne($khachHang->dai_ly_id);
            $str = 'Mã OTP xác minh giao dịch {{idgiaodich}}. Thanh toán  bài của khách '.$this->dataPost['khach_hang']['hoten'];
        }else{
            $giaodich->so_luot_xem = intval($this->dataPost['so_luot_xem']);
            $giaodich->don_gia = intval(str_replace(',','', $this->dataPost['don_gia']));
            $random.= $this->dataPost['dai_ly'];
            $giaodich->khach_hang_id = $this->dataPost['dai_ly'];
            $daiLyHienTai = User::findOne($this->dataPost['uid']);
            $str = 'Mã OTP xác minh giao dịch {{idgiaodich}} mua gói của đại lý ';
            $giaodich->khuyen_mai = intval(str_replace(',', '', $this->dataPost['khuyen_mai']));
            $giaodich->so_tien = $giaodich->don_gia * $giaodich->so_luot_xem;
            $giaodich->so_tien_giao_dich = $giaodich->so_tien - $giaodich->khuyen_mai;
        }

        $giaodich->ma_giao_dich = $random;
        $giaodich->user_id = $this->dataPost['uid']; // Dù là ai thực hiện giao dịch thì người thực hiện luôn là người đang đăng nhập
        // Nhưng nếu gửi email thì:
        // trường hợp khách hàng thanh toán tiền, mã giao dịch gửi về email của đại lý quản lý khách hàng này, phòng trường hợp admin thực hiện giao dịch bừa
        // Nếu là đại lý nạp tiền thì mã giao dịch gửi về email của người đang thực hiện giao dịch.
        $giaodich->loai_giao_dich = $this->dataPost['loai_giao_dich'];//GiaoDich::NAP_TIEN;
        $giaodich->ngay_giao_dich = $this->dataPost['ngay_thanh_toan'];
        $giaodich->save();

        $trangthaigiaodich = new TrangThaiGiaoDich();
        $trangthaigiaodich->giao_dich_id = $giaodich->id;
        $trangthaigiaodich->trang_thai = TrangThaiGiaoDich::CHO_XAC_MINH;
        $trangthaigiaodich->user_id = $this->dataPost['uid'];
        $trangthaigiaodich->save();

        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $noi_dung_xac_nhan_giao_dich = Cauhinh::findOne(['ghi_chu' => 'noi_dung_xac_nhan_giao_dich']);
        $noi_dung_xac_nhan_giao_dich = str_replace('{{id_giao_dich}}', $giaodich->id, $noi_dung_xac_nhan_giao_dich->content);
        $noi_dung_xac_nhan_giao_dich = str_replace('{{ma_giao_dich}}', $random, $noi_dung_xac_nhan_giao_dich);
        $noi_dung_xac_nhan_giao_dich = str_replace('{{thoi_gian_bat_dau}}', date("d/m/Y H:i"), $noi_dung_xac_nhan_giao_dich);

        myAPI::sendMailAmzon(
            $noi_dung_xac_nhan_giao_dich,
            ThanSoHoc::getEmailGui(),
            $daiLyHienTai->email, str_replace('{{idgiaodich}}', sprintf('%05d', $giaodich->id), $str)
        );

        return [
            'content' => "Mã giao dịch {$giaodich->id} đã được gửi về ".$daiLyHienTai->email.". Vui lòng truy cập email lấy mã giao dịch và điền vào ô bên cạnh!",
            'title' => 'Thông báo',
            'sendmagiaodich' => $giaodich->id
        ];
    }

    // dai-ly-nap-tien
    public function actionDaiLyNapTien(){
        if($this->dataPost['giao_dich'] == '')
            throw new HttpException(500, 'Mã xác minh không hợp lệ');
        else{
            GiaoDich::updateAll([
                'khuyen_mai' => intval(str_replace(',','',$this->dataPost['khuyen_mai'])),
                'so_tien_giao_dich' => intval(str_replace(',','',$this->dataPost['so_tien_giao_dich'])),
                'ma_giao_dich_khach_paste' => trim($this->dataPost['ma_giao_dich']),
            ], ['id' => $this->dataPost['giao_dich']]);
            $soLuotXem = intval(str_replace(',','', $this->dataPost['so_luot_xem']));
            $donGia = intval(str_replace(',','', $this->dataPost['don_gia']));
            $khuyenMai = intval(str_replace(',','', $this->dataPost['khuyen_mai']));
            $soTien = $donGia * $soLuotXem;
            $thanhTien = $soTien - $khuyenMai;

            $giaodich = QuanLyGiaoDich::findOne([
                'id' => $this->dataPost['giao_dich'],
                'ma_giao_dich' =>  trim($this->dataPost['ma_giao_dich']),
                'trang_thai' => 'Chờ xác minh'
            ]);
            $trangthaigiaodich = new TrangThaiGiaoDich();

            if(is_null($giaodich)){
                $trangthaigiaodich->trang_thai = TrangThaiGiaoDich::KHONG_THANH_CONG;
                $trangthaigiaodich->giao_dich_id = $this->dataPost['giao_dich'];
                $trangthaigiaodich->save();
                throw new HttpException(500,"Mã xác minh không hợp lệ");
            }
            else{
                $trangthaigiaodich->giao_dich_id = $giaodich->id;

                $khach_hang = User::findOne($giaodich->khach_hang_id);
                if((date("Y-m-d H:i", strtotime("+5 minutes", strtotime($giaodich->created))) < date("Y-m-d H:i"))){
                    $trangthaigiaodich->trang_thai = TrangThaiGiaoDich::KHONG_THANH_CONG;
                    $trangthaigiaodich->save();
                    throw new HttpException(500,"Mã xác thực đã quá hạn sử dụng, vui lòng tạo lại!");
                }
                else{
                    $trangthaigiaodich->trang_thai = TrangThaiGiaoDich::THANH_CONG;
                    $trangthaigiaodich->save();

                }

                // thanh toán đơn hàng
                $giaodichMuaLuotXem = new GiaoDich();
                $giaodichMuaLuotXem->khach_hang_id = $giaodich->khach_hang_id;
                $giaodichMuaLuotXem->loai_giao_dich = GiaoDich::THANH_TOAN_DON_HANG;
                $giaodichMuaLuotXem->so_tien_giao_dich = $giaodich->so_tien_giao_dich;
                $giaodichMuaLuotXem->giao_dich_nap_tien_id = $giaodich->id;
                $giaodichMuaLuotXem->user_id = $this->dataPost['uid'];
                $giaodichMuaLuotXem->ngay_giao_dich = $giaodich->ngay_giao_dich;
                $giaodichMuaLuotXem->so_luot_xem = $soLuotXem;
                $giaodichMuaLuotXem->don_gia = $donGia;
                $giaodichMuaLuotXem->so_tien = $soTien;
                $giaodichMuaLuotXem->khuyen_mai = $khuyenMai;
                $giaodichMuaLuotXem->so_tien_giao_dich = $thanhTien;
                $giaodichMuaLuotXem->so_bai_con_lai = $soLuotXem;
                $giaodichMuaLuotXem->mua_goi = 1;
                $giaodichMuaLuotXem->cho_phep_thong_ke = 1;
                if($giaodichMuaLuotXem->save()){
                    $trangthaigiaodich = new TrangThaiGiaoDich();
                    $trangthaigiaodich->giao_dich_id = $giaodichMuaLuotXem->id;
                    $trangthaigiaodich->trang_thai = TrangThaiGiaoDich::THANH_CONG;
                    $trangthaigiaodich->save();

                    $attrUpdate['so_lan_xem_con_lai'] = $khach_hang->so_lan_xem_con_lai + $soLuotXem;
                    $attrUpdate['da_chi'] = $khach_hang->da_chi + $giaodichMuaLuotXem->so_tien_giao_dich;
                    $khach_hang->updateAttributes($attrUpdate);
                }
                return ['content' => "Đã lưu giao dịch thành công! ".$soLuotXem.' lượt xem đã được cộng vào tài khoản '.$khach_hang->hoten, 'title' => 'Thông báo'];
            }
        }

    }

    // check-ma-giao-dich // hiện chưa dùng
    public function actionCheckMaGiaoDich(){
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $giaodich = QuanLyGiaoDich::findOne(['id' => $_POST['GiaoDich']['id'], 'ma_giao_dich' => trim($_POST['GiaoDich']['ma_giao_dich_khach_paste']), 'trang_thai' => 'Chờ xác minh']);
        if(is_null($giaodich))
            return ['loi' => 1, 'message' => '<span class="text-danger">Mã giao dịch không hợp lệ</span>'];
        else{
            $thoi_gian_tao_giao_dich = $giaodich->created;
            if((date("Y-m-d H:i", strtotime("-5 minutes")) > $thoi_gian_tao_giao_dich))
                return ['loi' => 1, 'message' => '<span class="text-danger">Mã này đã quá hạn, vui lòng tạo lại!</span>'];
            else
                return ['loi' => 0, 'message' => '<span class="text-success">Mã xác thực hợp lệ</span>'];
        }
    }

    // get-data
    public function actionGetData(){
        $data = QuanLyGiaoDich::find()
            ->select(['id', 'loai_giao_dich', 'trang_thai_giao_dich', 'nguoi_tao', 'khach_hang', 'ghi_chu',
                'ngay_giao_dich', 'so_luot_xem', 'created',
               'dien_thoai_khach_hang', 'don_gia', 'khuyen_mai']);

        if(!User::isViewAll($this->dataPost['uid']))
            $data = $data->andWhere('khach_hang_id = :id or user_id = :id', [':id' => $this->dataPost['uid']]);

        $totalCount = $data->count();
        $data = $data
            ->limit($this->dataPost['limit'])
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->orderBy(['id' => SORT_DESC])
            ->all();
        return [
            'results' => $data,
            'rows' => $totalCount,
        ];
    }
}
