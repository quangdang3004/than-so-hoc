<?php

namespace backend\controllers;

use yii\web\Controller;
use yii\web\HttpException;
use yii\web\User;

class ResetPasswordController extends Controller
{
    public function actionXacNhanQuenMatKhau($uid, $reset_password_token){
        $user = \common\models\User::findOne(['id' => $uid]);
        if ($reset_password_token == $user->password_reset_token){
            $user->password = $reset_password_token;

            return [
                'status' => 1,
                'message' => 'Thay đổi mật khẩu thành công, mật khẩu của bạn là '.$reset_password_token,
            ];
        } else {
            throw new HttpException(500, "Mã xác nhận không hợp lệ!");
        }
    }
}