<?php
/**
 * Created by PhpStorm.
 * User: hungluong
 * Date: 6/28/18
 * Time: 00:33
 */

namespace backend\controllers;


use backend\models\ChucNang;
use backend\models\PhanQuyen;
use backend\models\QuanLyPhanQuyen;
use backend\models\VaiTro;
use common\models\myAPI;
use yii\filters\AccessControl;
use yii\helpers\Json;

class PhanQuyenController extends CoreApiController
{
    public function behaviors()
    {

        $arr_action = ['index', 'get-phan-quyen', 'save'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('PhanQuyen', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    // get-phan-quyen
    public function actionGetPhanQuyen(){
        $chucnang = ChucNang::findAll(['nhom' => $this->dataPost['nhom_chuc_nang']['key']]);
        $vaitro = VaiTro::find()->all();
        $fields = [
            ['key' => 'chuc_nang', 'label' => 'Chức năng', 'width' => '50%']
        ];
        /** @var VaiTro $item */
        foreach ($vaitro as $item) {
            $fields[] = ['key' => $item->name, 'label' => $item->name, 'class' => 'text-center', 'width' => '1%'];
        }
        $phanquyen = [];
        $matrixPhanQuyen = [];
        $checked = [];
        $i = 0;
        /** @var ChucNang $item */
        foreach ($chucnang as $item) {
            /** @var VaiTro $item_vaitro */
            $itemPhanQuyen = ['chuc_nang' => $item->name, 'chuc_nang_id' => $item->id, 'chuc_nang_phan_quyen' => []];
            $itemPhanQuyenVaiTro = [];
            $matrixPhanQuyen[$i] = [];
            $j = 0;
            foreach ($vaitro as $item_vaitro) {
                $itemPhanQuyenVaiTro[] = [
                    'vai_tro_id' => $item_vaitro->id,
                    'checked' => !is_null(PhanQuyen::findOne(['chuc_nang_id' => $item->id, 'vai_tro_id' => $item_vaitro->id]))
                ];
                $matrixPhanQuyen[$i][$j] = "{$item->id}_{$item_vaitro->id}";
                $checked[$i][$j] =  !is_null(PhanQuyen::findOne(['chuc_nang_id' => $item->id, 'vai_tro_id' => $item_vaitro->id])) ? "{$item->id}_{$item_vaitro->id}" : '';
                $j++;
            }
            $i++;
            $itemPhanQuyen['chuc_nang_phan_quyen'] = $itemPhanQuyenVaiTro;
            $phanquyen[] = $itemPhanQuyen;
        }

        return [
            'fields' => $fields,
            'phanQuyenMatrix' => $phanquyen,
            'maTrixPhanQuyen' => $matrixPhanQuyen,
            'checked' => $checked
        ];
    }

    public function actionSave(){
        $quanlyphanquyen = QuanLyPhanQuyen::findAll(['nhom' => $this->dataPost['nhom_chuc_nang']['key']]);
        foreach ($quanlyphanquyen as $item) {
            PhanQuyen::deleteAll(['id' => $item->id]);
        }

        if(isset($this->dataPost['checked']['matrix']))
            foreach ($this->dataPost['checked']['matrix'] as $item)
                foreach ($item as $value) {
                    if($value != ''){
                        $arr = explode('_', $value);
                        $phanquyen = new PhanQuyen();
                        $phanquyen->chuc_nang_id = $arr[0];
                        $phanquyen->vai_tro_id = $arr[1];
                        $phanquyen->save();
                    }
                }

        return [
            'message' => 'Đã lưu phân quyền thành công'
        ];
    }
}
