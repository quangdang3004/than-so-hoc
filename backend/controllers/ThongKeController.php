<?php


namespace backend\controllers;


use app\models\QuanLyDaiLy;
use backend\models\GiaoDich;
use backend\models\TrangThaiGiaoDich;
use common\models\myAPI;
use common\models\ThanSoHoc;
use common\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;

class ThongKeController extends CoreApiController
{
    public function behaviors()
    {

        $arr_action = ['index', 'thong-ke-doanh-so-dai-ly', 'xem-chi-tiet-loi-nhuan'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('ThongKe', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    public function actionBackupThongKeDoanhSoDaiLy(){
        $tongTienDauTu = 0;
        $tongTienDoanhSo = 0;
        $tongLoiNhuan = 0;

        if(!isset($this->dataPost['thong_ke']))
            $daiLyArr = QuanLyDaiLy::findAll(['id' => $this->dataPost['dai_ly']]);
        else{
            if($this->dataPost['dai_ly']['key'] == 'Tất cả'){
                if(User::isViewAll($this->dataPost['uid']))
                    $daiLyArr = QuanLyDaiLy::find()->all();
                else
                    $daiLyArr = QuanLyDaiLy::findAll(['id' => $this->dataPost['uid']]);
            }else
                $daiLyArr = QuanLyDaiLy::findAll(['id' => $this->dataPost['dai_ly']['key']]);
        }
        $arrKetQua = [];
        $ngayGiaoDich = [];
        $stt = 0;
        $giaVonBanDau = [];
        $soGiaoDich = [];

        $soLanBan = [];
        $giaBanBanDau = [];

        /** @var QuanLyDaiLy $daiLy */
        foreach ($daiLyArr as $daiLy) {
            if($this->dataPost['loai_thong_ke']['key'] == 'Theo ngày'){
                $lichSuGiaoDich = GiaoDich::find()
                    ->andFilterWhere(['>=', 'date(ngay_giao_dich)', $this->dataPost['tu_ngay']])
                    ->andFilterWhere(['<=', 'date(ngay_giao_dich)', $this->dataPost['den_ngay']])
                    ->andFilterWhere(['trang_thai_giao_dich' => 'Thành công'])
                    ->andFilterWhere(['cho_phep_thong_ke' => 1])
                    ->andFilterWhere(['loai_giao_dich' => GiaoDich::THANH_TOAN_DON_HANG])
                    ->andWhere('khach_hang_id =:id or user_id = :id', [':id' => $daiLy->id])
                    ->orderBy(['ngay_giao_dich' => SORT_ASC])
                    ->all();

                /** @var GiaoDich $item */
                foreach ($lichSuGiaoDich as $item) {
                    if(!isset($ngayGiaoDich[$item->ngay_giao_dich])){
                        $stt++;
                        $ngayGiaoDich[$item->ngay_giao_dich] = $stt;
                        $arrKetQua[$stt-1] = [
                            'stt' => $stt,
                            'dai_ly' => $daiLy->hoten,
                            'ma_dai_ly' => $daiLy->ma_dai_ly,
                            'dien_thoai' => $daiLy->dien_thoai,
                            'ngay' => date("d/m/Y", strtotime($item->ngay_giao_dich)),
                            'so_bai' => 0,
                            'gia_von' => 0,
                            'khuyen_mai' => 0,
                            'so_tien' => 0,
                            'so_bai_doanh_thu' => 0,
                            'gia_von_doanh_thu' => 0,
                            'khuyen_mai_doanh_thu' => 0,
                            'so_tien_doanh_thu' => 0,
                            'loi_nhuan' => 0,
                        ];
                        $giaVonBanDau[$stt-1] = 0;
                        $soGiaoDich[$stt-1] = 0;
                        $soLanBan[$stt-1] = 0;
                        $giaBanBanDau[$stt-1] = 0;
                    }else
                        $stt = $ngayGiaoDich[$item->ngay_giao_dich];

                    if($item->khach_hang_id == $daiLy->id){
                        $giaVonBanDau[$stt - 1] += intval($item->don_gia);
                        $soGiaoDich[$stt - 1]++;
                        $arrKetQua[$stt-1]['so_bai'] += $item->so_luot_xem;
                        $arrKetQua[$stt-1]['so_tien'] += $item->so_tien_giao_dich;
                        $arrKetQua[$stt-1]['khuyen_mai'] += $item->khuyen_mai;
                        $arrKetQua[$stt-1]['gia_von'] = $giaVonBanDau[$stt - 1]/$soGiaoDich[$stt - 1];
                        $tongTienDauTu += $item->so_tien_giao_dich;
                    }
                    if($item->user_id == $daiLy->id){
                        $khachHang = $item->khachHang;
                        $giaBanBanDau[$stt-1]+= intval($khachHang->so_tien);
                        $soLanBan[$stt-1]++;
                        $arrKetQua[$stt-1]['so_bai_doanh_thu'] += 1;
                        $arrKetQua[$stt-1]['so_tien_doanh_thu'] += $khachHang->so_tien_can_thanh_toan;
                        $arrKetQua[$stt-1]['khuyen_mai_doanh_thu'] += $khachHang->khuyen_mai;
                        $arrKetQua[$stt-1]['gia_von_doanh_thu'] = $giaBanBanDau[$stt-1]/$soLanBan[$stt-1];
                        $arrKetQua[$stt-1]['loi_nhuan']  += intval($khachHang->loi_nhuan);
                        $tongTienDoanhSo += $khachHang->so_tien_can_thanh_toan;
                        $tongLoiNhuan += $khachHang->loi_nhuan;
                    }
                }

            }else{
                $thangThongKeDau = $this->dataPost['tu_thang']['key'];
                $namThongKeDau = $this->dataPost['tu_nam'];
                $thangThongKeCuoi = $this->dataPost['den_thang']['key'];
                $namThongKeCuoi = $this->dataPost['den_nam'];
                $stt++;
                do{
                    $dataTienDauTu = ThanSoHoc::getThongKeDauTu($daiLy->id, $thangThongKeDau, $namThongKeDau);
                    $dataTienDoanhSoLoiNhuan = ThanSoHoc::getThongKeDoanhSoDaiLy($daiLy->id, $thangThongKeDau, $namThongKeDau);
                    $arrKetQua[] = [
                        'dai_ly' => $daiLy->hoten,
                        'ma_dai_ly' => $daiLy->ma_dai_ly,
                        'dien_thoai' => $daiLy->dien_thoai,
                        'stt' => $stt,
                        'ngay' => "Tháng {$thangThongKeDau}/{$namThongKeDau}",
                        'so_bai' => $dataTienDauTu['so_bai'],
                        'gia_von' => round($dataTienDauTu['gia_von']),
                        'khuyen_mai' => $dataTienDauTu['khuyen_mai'],
                        'so_tien' => $dataTienDauTu['tien_dau_tu'],
                        'so_bai_doanh_thu' => $dataTienDoanhSoLoiNhuan['so_bai'],
                        'gia_von_doanh_thu' => $dataTienDoanhSoLoiNhuan['so_tien'],
                        'khuyen_mai_doanh_thu' => $dataTienDoanhSoLoiNhuan['khuyen_mai'],
                        'so_tien_doanh_thu' => $dataTienDoanhSoLoiNhuan['doanh_so'],
                        'loi_nhuan' => $dataTienDoanhSoLoiNhuan['loi_nhuan'],
                    ];
                    $thangTiepTheo = date("Y-m-d", strtotime("+1 months", strtotime("{$namThongKeDau}-{$thangThongKeDau}-1")));
                    $thangThongKeDau = date("m", strtotime($thangTiepTheo));
                    $namThongKeDau = date("Y", strtotime($thangTiepTheo));
                    $tongTienDauTu += $dataTienDauTu['tien_dau_tu'];
                    $tongTienDoanhSo += $dataTienDoanhSoLoiNhuan['doanh_so'];
                    $tongLoiNhuan += $dataTienDoanhSoLoiNhuan['loi_nhuan'];
                    $stt++;
                }while(date("Y-m-d", strtotime("{$namThongKeDau}-{$thangThongKeDau}-1")) <= date("Y-m-d", strtotime("{$namThongKeCuoi}-{$thangThongKeCuoi}-1")));
            }
        }

        return [
            'ket_qua' => $arrKetQua,
            'tongTienDauTu' => $tongTienDauTu,
            'tongTienDoanhSo' => $tongTienDoanhSo,
            'tongLoiNhuan' => $tongLoiNhuan
        ];
    }

    // thong-ke-doanh-so-dai-ly
    public function actionThongKeDoanhSoDaiLy(){
        $tongSoBaiDaXem = 0;
        $tongSoBaiDaMua = 0;
        $tongSoBaiDaBan = 0;

        if(!isset($this->dataPost['thong_ke']))
            $daiLyArr = QuanLyDaiLy::findAll(['id' => $this->dataPost['dai_ly']]);
        else{
            if($this->dataPost['dai_ly']['key'] == 'Tất cả'){
                if(User::isViewAll($this->dataPost['uid']))
                    $daiLyArr = QuanLyDaiLy::find()->all();
                else
                    $daiLyArr = QuanLyDaiLy::findAll(['id' => $this->dataPost['uid']]);
            }else
                $daiLyArr = QuanLyDaiLy::findAll(['id' => $this->dataPost['dai_ly']['key']]);
        }
        $arrKetQua = [];
        $stt = 0;

        /** @var QuanLyDaiLy $daiLy */
        foreach ($daiLyArr as $daiLy) {
            if($this->dataPost['loai_thong_ke']['key'] == 'Theo ngày'){
                $lichSuGiaoDich = GiaoDich::find()
                    ->andFilterWhere(['>=', 'date(ngay_giao_dich)', $this->dataPost['tu_ngay']])
                    ->andFilterWhere(['<=', 'date(ngay_giao_dich)', $this->dataPost['den_ngay']])
                    ->andWhere('(khach_hang_id =:id or user_id = :id) ', [':id' => $daiLy->id])
                    ->orderBy(['ngay_giao_dich' => SORT_ASC])
                    ->groupBy(['ngay_giao_dich', 'loai_giao_dich', 'user_id'])
                    ->all();
                $ketQuaDaiLy = [];
                /** @var GiaoDich $item */
                foreach ($lichSuGiaoDich as $item) {
                    if(!isset($ketQuaDaiLy[$item->ngay_giao_dich])){
                        $ketQuaDaiLy[$item->ngay_giao_dich]=[
                            'mua_bai' => 0,
                            'xem_bai' => 0
                        ];
                    }

                    if($item->loai_giao_dich == GiaoDich::MUA_BAI){
                        if($item->user_id == $daiLy->id)
                            $ketQuaDaiLy[$item->ngay_giao_dich]['ban_goi'] = $item->so_luot_xem;
                        else
                            $ketQuaDaiLy[$item->ngay_giao_dich]['mua_bai'] = $item->so_luot_xem;
                    }
                    else if($item->loai_giao_dich == GiaoDich::XEM_BAI)
                        $ketQuaDaiLy[$item->ngay_giao_dich]['xem_bai'] = $item->so_luot_xem;
                }

                foreach ($ketQuaDaiLy as $ngay_giao_dich => $item) {
                    $arrKetQua[] = [
                        'stt' => $stt + 1,
                        'ngay' => date("d/m/Y", strtotime($ngay_giao_dich)),
                        'dai_ly' => $daiLy->hoten,
                        'mua_bai' =>  $item['mua_bai'],
                        'xem_bai' =>  $item['xem_bai'],
                        'ban_goi' =>  $item['ban_goi'],
                    ];
                    $stt++;
                    $tongSoBaiDaXem += intval($item['xem_bai']);
                    $tongSoBaiDaMua += intval($item['mua_bai']);
                    $tongSoBaiDaBan += intval($item['ban_goi']);
                }
            }
            else{
                $thangThongKeDau = $this->dataPost['tu_thang']['key'];
                $namThongKeDau = $this->dataPost['tu_nam'];
                $thangThongKeCuoi = $this->dataPost['den_thang']['key'];
                $namThongKeCuoi = $this->dataPost['den_nam'];
                $stt++;
                do{
                    $dataSoBai = ThanSoHoc::getThongKeBai($daiLy->id, $thangThongKeDau, $namThongKeDau);
//                    if(!isset($dataSoBai['mua_bai'])){
//                        VarDumper::dump($dataSoBai, 10, true); exit;
//                    }
                    $muaBai = isset($dataSoBai['mua_bai']) ? $dataSoBai['mua_bai'] : 0;
                    $banGoi = isset($dataSoBai['ban_goi']) ? $dataSoBai['ban_goi'] : 0;
                    $xemBai = isset($dataSoBai['xem_bai']) ? $dataSoBai['xem_bai'] : 0;
                    $arrKetQua[] = [
                        'stt' => $stt,
                        'mua_bai' => $muaBai,
                        'ban_goi' => $banGoi,
                        'xem_bai' => $xemBai,
                        'dai_ly' => $daiLy->hoten,
                        'ngay' => sprintf('%02d', $thangThongKeDau).'-'.$namThongKeDau
                    ];
                    $thangTiepTheo = date("Y-m-d", strtotime("+1 months", strtotime("{$namThongKeDau}-{$thangThongKeDau}-1")));
                    $thangThongKeDau = date("m", strtotime($thangTiepTheo));
                    $namThongKeDau = date("Y", strtotime($thangTiepTheo));
                    $tongSoBaiDaXem += $xemBai;
                    $tongSoBaiDaBan += $banGoi;
                    $tongSoBaiDaMua += $muaBai;
                    $stt++;
                }while(date("Y-m-d", strtotime("{$namThongKeDau}-{$thangThongKeDau}-1")) <= date("Y-m-d", strtotime("{$namThongKeCuoi}-{$thangThongKeCuoi}-1")));
            }
        }

        return [
            'ket_qua' => $arrKetQua,
            'tongSoBaiDaXem' => $tongSoBaiDaXem,
            'tongSoBaiDaMua' => $tongSoBaiDaMua,
            'tongSoBaiDaBan' => $tongSoBaiDaBan,
        ];
    }

    // xem-chi-tiet-loi-nhuan
    public function actionXemChiTietLoiNhuan(){
        $daiLy = User::findOne($this->dataPost['dai_ly']);

        if(strpos($this->dataPost['ngay'], 'Tháng') === false){
            $ngay_from = myAPI::convertDateSaveIntoDb($this->dataPost['ngay']);
            $ngay_to = $ngay_from;
            $title = 'Chi tiết lợi nhuận ngày '.$this->dataPost['ngay'].', đại lý '.$daiLy->hoten;
        }else{
            // Tháng 02/2021 => $arr[0] = '02', $arr[1] = '2021'
            $arr = explode('/',trim(str_replace('Tháng', '', $this->dataPost['ngay'])));
            $ngay_from = date("Y-m-d", strtotime("{$arr[1]}-{$arr[0]}-1"));
            $ngay_to = date("Y-m-t", strtotime($ngay_from));
            $title = 'Chi tiết lợi nhuận '.$this->dataPost['ngay'].', đại lý '.$daiLy->hoten;
        }

        $khachHang = User::find()
                ->andFilterWhere(['dai_ly_id' => $this->dataPost['dai_ly']])
                ->andFilterWhere(['>=', 'date(ngay_thanh_toan_lan_dau)', $ngay_from])
                ->andFilterWhere(['<=', 'date(ngay_thanh_toan_lan_dau)', $ngay_to])
                ->all();

        $data = [];
        /** @var  $item */
        foreach ($khachHang as $item) {
            $goi = GiaoDich::findOne($item->goi_id);
            $data[] = [
                'ngay' => date("d/m/Y", strtotime($item->ngay_thanh_toan_lan_dau)),
                'khach_hang' => $item->hoten,
                'gia_von' => $goi->don_gia,
                'khuyen_mai' => round($goi->khuyen_mai / $goi->so_luot_xem),
                'so_tien' => $goi->don_gia - round($goi->khuyen_mai / $goi->so_luot_xem),
                'gia_ban' => $item->so_tien,
                'khuyen_mai_gia_ban' => $item->khuyen_mai,
                'so_tien_thanh_toan' => $item->so_tien_can_thanh_toan,
                'loi_nhuan' => $item->so_tien_can_thanh_toan - ($goi->don_gia - round($goi->khuyen_mai / $goi->so_luot_xem))
            ];
        }
        return [
            'title' => $title,
            'chiTietLoiNhuan' => $data
        ];
    }
}
