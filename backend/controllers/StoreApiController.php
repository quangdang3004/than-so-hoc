<?php
namespace backend\controllers;
use common\models\ThanSoHoc;
use common\models\User;
use yii\web\Cookie;
use yii\web\HttpException;
class StoreApiController extends CoreApiController
{
    public function actionLogin(){
        if(!isset($_POST['username']))
            throw new HttpException(500, 'Không có thông tin tên đăng nhập');
        else if(!isset($_POST['password']))
            throw new HttpException(500, 'Không có thông tin mật khẩu');
        else{
            if(empty($_POST['username']) || empty($_POST['password'])){
                throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại và mật khẩu không được để trống');
            }else{
                /** @var \common\models\User $user */
                $user = \common\models\User::find()
                    ->select(['id', 'username', 'password_hash', 'email', 'auth_key', 'VIP', 'vi_dien_tu', 'so_lan_xem_con_lai'])
                    ->andWhere('username = :u or dien_thoai = :u', [
                        ':u' => $_POST['username']
                    ])
                    ->andFilterWhere(['status' => 10])
                    ->one();
                if(is_null($user)){
                    throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại không đúng');
                }else{
                    if(\Yii::$app->security->validatePassword($_POST['password'], $user->password_hash)){
                        $auth = \Yii::$app->security->generateRandomString(32);
                        $user->updateAttributes(['auth_key' => $auth]);
////
                        $cookies = \Yii::$app->response->cookies;
                        $cookies->add(new Cookie([
                            'name' => 'token',
                            'value' => $auth
                        ]));
                        $cookies->add(new Cookie([
                            'name' => 'username',
                            'value' => \Yii::$app->security->generateRandomString()
                        ]));
                        $cookies->add(new Cookie([
                            'name' => 'userId',
                            'value' => $user->username
                        ]));
//                        setcookie("token", $auth, time() + 3600 * 24, ".vanchuyenmocphat.com");
//                        setcookie("username", \Yii::$app->security->generateRandomString(), time() + (60 * 60 * 24 * 30), ".vanchuyenmocphat.com");
//                        setcookie("userId", $user->username, time() + (60 * 60 * 24 * 30), ".vanchuyenmocphat.com");
                        return [
                            'message' => 'Đăng nhập thành công',
                            'uid' => $user->id,
                            'auth' => $auth,
                            'user' => User::find()
                                ->select(['id', 'username', 'password_hash', 'email', 'auth_key', 'VIP', 'vi_dien_tu', 'so_lan_xem_con_lai'])
                                ->andFilterWhere(['id' => $user->id])
                                ->one(),
                            'tien_dau_tu' => ThanSoHoc::getTienDauTu($user->id, date('m'), date("Y")),
                            'doanh_so' => ThanSoHoc::getDoanhSo($user->id, date("m"), date("Y"))
                        ];
                    }
                    else
                        throw new HttpException(500, 'Mật khẩu không đúng');
                }
            }
        }
    }
    public function actionLogout(){
        $cookies = \Yii::$app->response->cookies;
        $cookies->remove('token');
        $cookies->remove('username');
        $cookies->remove('userId');
        return [
            'message' => 'Success',
            'status' => 200,
            'content' => 'Đã đăng xuất thành công'
        ];
    }
}
