<?php

namespace backend\controllers;

use app\models\Images;
use backend\models\Cauhinh;
use backend\models\ChiSo;
use backend\models\GiaoDich;
use backend\models\ThongBao;
use backend\models\UserVaiTro;
use backend\models\Vaitrouser;
use backend\models\Voucher;
use backend\models\YNghiaCacConSo;
use backend\models\YNghiaChiSo;
use common\models\myAPI;
use common\models\ThanSoHoc;
use common\models\User;
use PhpOffice\PhpSpreadsheet\Shared\Trend\PolynomialBestFit;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Cookie;
use yii\web\HttpException;

class RestfulApiController extends CoreApiController
{

    public function actionLogin(){
        if(!isset($_POST['dien_thoai']))
            throw new HttpException(500, 'Không có thông tin số điện thoại đăng nhập');
        else if(!isset($_POST['password']))
            throw new HttpException(500, 'Không có thông tin mật khẩu');
        else{
            if(empty($_POST['dien_thoai']) || empty($_POST['password'])){
                throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại và mật khẩu không được để trống');
            }else if($_POST['username']=="" || $_POST['password']==""){
                throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại và mật khẩu không được để trống');
            }else{
                /** @var \common\models\User $user */
                $user = \common\models\User::find()
                    ->select(['id', 'username', 'password_hash', 'email', 'auth_key', 'VIP', 'vi_dien_tu', 'so_lan_xem_con_lai'])
                    ->andWhere('username = :u or dien_thoai = :u', [
                        ':u' => $_POST['dien_thoai']
                    ])
                    ->andFilterWhere(['status' => 10])
                    ->one();
                if(is_null($user)){
                    throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại không đúng');
                }else{
                    if(\Yii::$app->security->validatePassword($_POST['password'], $user->password_hash)){
                        $auth = \Yii::$app->security->generateRandomString(32);
                        $user->updateAttributes(['auth_key' => $auth]);
////
                        $cookies = \Yii::$app->response->cookies;
                        $cookies->add(new Cookie([
                            'name' => 'token',
                            'value' => $auth
                        ]));
                        $cookies->add(new Cookie([
                            'name' => 'username',
                            'value' => \Yii::$app->security->generateRandomString()
                        ]));
                        $cookies->add(new Cookie([
                            'name' => 'userId',
                            'value' => $user->username
                        ]));
//                        setcookie("token", $auth, time() + 3600 * 24, ".vanchuyenmocphat.com");
//                        setcookie("username", \Yii::$app->security->generateRandomString(), time() + (60 * 60 * 24 * 30), ".vanchuyenmocphat.com");
//                        setcookie("userId", $user->username, time() + (60 * 60 * 24 * 30), ".vanchuyenmocphat.com");
                        return [
                            'message' => 'Đăng nhập thành công',
                            'uid' => $user->id,
                            'auth' => $auth,
                            'user' => User::find()
                                ->select(['id', 'username', 'email', 'auth_key', 'VIP', 'vi_dien_tu', 'so_lan_xem_con_lai'])
                                ->andFilterWhere(['id' => $user->id])
                                ->one(),
                            'tien_dau_tu' => ThanSoHoc::getTienDauTu($user->id, date('m'), date("Y")),
                            'doanh_so' => ThanSoHoc::getDoanhSo($user->id, date("m"), date("Y"))
                        ];
                    }
                    else
                        throw new HttpException(500, 'Mật khẩu không đúng');
                }
            }
        }
    }

    public function actionDangKi(){
        if(!isset($_POST['hoten']) || !isset($_POST['username']) || !isset($_POST['password']) || !isset($_POST['dien_thoai']) || !isset($_POST['email'])){
            throw new HttpException(500,'Vui lòng truyền đủ tham số');
        }
        if($_POST['hoten'] ==  "")
                throw new HttpException(500,'Vui lòng điền họ tên');
        if($_POST['username'] ==  "")
            throw new HttpException(500,'Vui lòng điền username');
        if($_POST['password'] ==  "")
            throw new HttpException(500,'Vui lòng điền password');
        if($_POST['dien_thoai'] ==  "")
            throw new HttpException(500,'Vui lòng điền điện thoại');
        if($_POST['email'] ==  "")
            throw new HttpException(500,'Vui lòng điền email');
        $user = User::findOne(['username',$_POST['username']]);
        if(is_null($user)){
            if (strlen($_POST['dien_thoai']) != 10){
                throw new HttpException(500, 'Số điện thoại không hợp lệ');
            } else {
                $user = User::findOne(['dien_thoai',$_POST['dien_thoai']]);
                if(is_null($user))
                {
                    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                        throw new HttpException(500, 'Địa chỉ email không hợp lệ!');
                    } else {
                        $user = User::findOne(['email',$_POST['email']]);
                        if(is_null($user))
                        {
                            $user = new User();
                            $user->hoten = $_POST['hoten'];
                            $user->username = $_POST['username'];
                            $user->email = $_POST['email'];
                            $user->password_hash = \Yii::$app->security->generatePasswordHash($_POST['password']);
                            $user->dien_thoai = $_POST['dien_thoai'];
                            $user->dai_ly_id =1;
                            if($user->save()){
                                $vaitro =  new Vaitrouser();
                                $vaitro->vaitro_id = 2;
                                $vaitro->user_id = $user->id;
                                $vaitro->save();
                            }
                            return [
                                'message' => 'Đăng kí thành công !',
                            ];
                        }
                    }
                }
            }
        }


    }

    public function actionGetDataBaiViet(){
        $data = ThongBao::find()->andFilterWhere(['type'=>'Bài đăng'])->all();
        $thongBao =  ThongBao::find()
            ->andFilterWhere(['type'=>'Thông báo'])
            ->andFilterWhere(['<=','start_date',date('y-m-d',time())])
            ->andFilterWhere(['>=','end_date',date('y-m-d',time())])
            ->all();
        return [
            'baiDang' => $data,
            'thongBao' => $thongBao,
        ];
    }

    public function actionXemLuanGiai(){
        $hoTen ='';
        if ( isset($_POST['ho_ten'])){
            $hoTen = $_POST['ho_ten'];
        }else
            throw new HttpException(500,'Vui lòng điền họ tên');
        if ( isset($_POST['date'])){
            $ngaySinh = $_POST['date'];
            if($ngaySinh == '')
                throw new HttpException(500,'Vui lòng điền thông tin ngày sinh');
        }
        $ngaySinh = explode('/', $ngaySinh);
        if(count($ngaySinh) != 3)
            throw new HttpException(500, 'Ngày sinh không hợp lệ');
        $ngaySinh = implode('-', $ngaySinh);
        $ngaySinh = date('Y-m-d', strtotime($ngaySinh));

        if ( isset($_POST['dia_chi_chi_tiet'])){
            if($_POST['dia_chi_chi_tiet'] == '')
                throw new HttpException(500,'Vui lòng điền thông địa chỉ');
        }

        if (isset($hoTen))
            if (trim($hoTen) == '') {
                throw new HttpException(500, 'Vui lòng nhập họ tên');
        } else {
//            $nguon_tra_cuu = str_replace('http:', '', str_replace('https:', '', str_replace('/', '', $_SERVER['HTTP_REFERER'])));
            $daiLy = User::findOne(['sercurity_code' => 'c909e951ce946e430c5c57eb90100418']);
            if (is_null($daiLy))
                throw new HttpException(500, 'Không tìm thấy đại lý tương ứng');
            else {
                $user = User::findOne([
//                        'dien_thoai' => $dataPost['dien_thoai'],
                    'hoten' => $hoTen,
                    'ngay_sinh' => date("Y-m-d", strtotime($ngaySinh))
                ]);
                if (is_null($user))
                    $user = new User();
                else {
                    $chiSoCu = ChiSo::findAll(['khach_hang_id' => $user->id]);
                    foreach ($chiSoCu as $item) {
                        $item->delete();
                    }
                }
                $user->hoten = $hoTen;
                $user->dia_chi = isset($_POST['dia_chi_chi_tiet']) ? $_POST['dia_chi_chi_tiet'] : null;
                $user->ngay_sinh = $ngaySinh;
                $user->khach_hang = 1;
                $user->so_tien = intval(Cauhinh::findOne(['ghi_chu' => 'chi_phi_mot_lan_xem_online'])->content);
                $user->so_tien_can_thanh_toan = intval(Cauhinh::findOne(['ghi_chu' => 'chi_phi_mot_lan_xem_online'])->content);
//                $user->nguon_tra_cuu = $nguon_tra_cuu;
                $user->dai_ly_id = $daiLy->id;
                $user->auth_key = \Yii::$app->security->generateRandomString();

                $voucher_id = null;
                // Kiểm tra có phải tra hướng nghiệp không
                if (isset($dataPost['ma_so_cua_ban'])) {
                    if (trim($dataPost['ma_so_cua_ban']) != '') {
                        $voucher = Voucher::find()
                            ->andFilterWhere(['ma' => trim($dataPost['ma_so_cua_ban'])])
                            ->andWhere('(ngay_su_dung is null) or ma = :m', [':m' => 'TSHN21'])
                            ->one();
                        if (!is_null($voucher)) {
                            $voucher->updateAttributes(['ngay_su_dung' => date("Y-m-d H:i:s")]);
                            $voucher_id = $voucher->id;
                        }
                    }
                }
                $user->voucher_id = $voucher_id;
                if ($user->save()) {
                    $dataChiSo = [];
                    if (isset($dataPost['ma_so_cua_ban'])) {
                        if (trim($dataPost['ma_so_cua_ban']) == '')
                            throw new HttpException(500, 'Vui lòng nhập mã số của bạn');
                        else {
                            if (is_null($user->voucher_id))
                                throw new HttpException(500, 'Mã số của bạn không đúng, vui lòng nhập lại');
                            else {
                                $cacLoaiChiSo = [
                                    'Số Đường Đời' => 'Đường đời',
                                    'Số Vận Mệnh' => 'Vận mệnh',
//                                        'Số Trưởng Thành' => 'Số trưởng thành',
                                    'Số Ngày Sinh' => 'Chỉ số ngày sinh',
//                                        'Số Tâm Hồn' => 'Số tâm hồn'
                                ];
                                foreach ($cacLoaiChiSo as $label => $type) {
                                    $chiSo = ChiSo::findOne(['khach_hang_id' => $user->id, 'loai_chi_so' => $type]);
                                    $yNghia = YNghiaChiSo::findOne(['chi_so_id' => $chiSo->id]);
                                    $chiSo = $yNghia->yNghia->chi_so;
                                    if (intval($chiSo) > 10) {
                                        $tong = 0;
                                        for ($i = 0; $i < strlen(strval($chiSo)); $i++)
                                            $tong += intval(strval($chiSo)[$i]);
                                    } else
                                        $tong = intval($chiSo);
                                    $yNghiaConSo = YNghiaCacConSo::findOne(['chi_so' => $tong, 'nhom' => 'Chỉ số hướng nghiệp']);
                                    if (!isset($yNghiaConSo)) {
                                        VarDumper::dump($yNghia->yNghia->chi_so, 10, true);
                                        exit;
                                    }
                                    $dataChiSo[] = [
                                        'loai_chi_so' => $label . ': ' . $yNghiaConSo->chi_so,
                                        'chu_de' => $yNghiaConSo->chu_de,
                                        'ghi_chu' => str_replace('{{new_line}}', '', $yNghiaConSo->noi_dung)
                                    ];
                                }
                            }
                        }

                    } else {
                        $ten_chi_so = [
                            'duong-doi' => 'Số đường đời',
                            'chi-so-van-menh' => 'Số vận mệnh',
                            'nam-than-so' => 'Năm thần số'
                        ];

                        $chiSo = ChiSo::find()
                            ->select(['loai_chi_so', 'noi_dung', 'ghi_chu', 'id'])
                            ->andFilterWhere(['khach_hang_id' => $user->id])
                            ->orderBy(['thu_tu' => SORT_ASC])
                            ->andFilterWhere(['in', 'loai_chi_so', ['Đường đời', 'Vận mệnh', 'Năm thần số']])
                            ->all();

                        /**
                         * @var int $index
                         * @var ChiSo $item
                         */
                        foreach ($chiSo as $index => $item) {
                            $yNghiaConSo = YNghiaChiSo::findAll(['chi_so_id' => $item->id]);
                            if (count($yNghiaConSo) == 1) {
                                $yNghia = $yNghiaConSo[0]->yNghia;
                                $dataChiSo[] = [
                                    'loai_chi_so' => $ten_chi_so[myAPI::createCode($yNghia->nhom)] . ': ' . $yNghia->chi_so,
                                    'chu_de' => $yNghia->chu_de,
                                    'ghi_chu' => ((str_replace(['{{new_line}}'], '', $yNghia->noi_dung))),
                                ];
                            } else if (count($yNghiaConSo) > 1) {
                                $nhom = $yNghiaConSo[0]->yNghia->nhom;
                                $str = [];
                                foreach ($yNghiaConSo as $index2 => $itemYNghiaConSo) {
                                    $yNghia = $itemYNghiaConSo->yNghia;
                                    $str[] = '<strong>' . mb_strtoupper($nhom) . ' ' . ($index2 + 1) . ' - số ' . $yNghia->chi_so . ': ' . $yNghia->chu_de . '</strong><br/>' . $yNghia->noi_dung;
                                }
                                $dataChiSo[] = [
                                    'loai_chi_so' => $ten_chi_so[myAPI::createCode($nhom)],
                                    'chu_de' => '',
                                    'ghi_chu' =>($str),
                                ];
                            } else {
                                $dataChiSo[] = [
                                    'loai_chi_so' => $item->loai_chi_so,
                                    'chu_de' => '',
                                    'ghi_chu' => ($item->noi_dung)
                                ];
                            }
                        }
                    }

                    return [
//                        'source' => $_SERVER['HTTP_REFERER'],
                        'title' => 'Thông tin luận giải ' . $user->hoten,
                        'luan_giai' => $dataChiSo,
                        'result' => 'success',
                        'success' => 1
                    ];
                } else
                   throw new HttpException(500,  Html::errorSummary($user));
            }
        }
    }

    public function actionQuenMatKhau(){
        $user = $_POST['username'];
        if (isset($user)){
            if ($user == ''){
                throw new HttpException(500, "Vui lòng nhập tên người dùng");
            }
        }
        $user = User::findOne(['username' => $user]);
        if (!isset($user)){
            throw new HttpException(500, 'Không tồn tại người dùng!');
        } else {
            $email = $user->email;
            if (!isset($email)){
                throw new HttpException(500, 'Vui lòng cập nhật email cho tài khoản!');
            } else if (filter_var($email, FILTER_VALIDATE_EMAIL)){
                $reset_token = $this->generateRandomString(8);
                myAPI::sendEmail(" Mật khẩu mới của bạn là: {$reset_token}",
                    'hungdd@minhhien.com.vn', $email, 'Mã xác nhận tài khoản');

                $password = \Yii::$app->getSecurity()->generatePasswordHash($reset_token);
                $user->updateAttributes(['password_reset_token' => $reset_token]);
                $user->updateAttributes(['password_hash' => $password]);
                return [
                    'status' => 1,
                    'message' => "Vui lòng kiểm tra email {$email} để nhận mã xác nhận đổi mật khẩu!"
                ];
            } else {
                throw new HttpException(500, 'Email không hợp lệ!');
            }

        }
    }

    public  function actionManHinhTrangChu(){
        $images = Images::findAll(['type' => 'Hoạt động']);
        $link_image = [];
        foreach ($images as $index => $item){
            $link_image[$index] = $item->link_image;
        }

        $partner = Images::findAll(['type' => 'Đối tác']);
        $link_partner = [];
        foreach ($partner as $index => $item){
            $link_partner[$index] = $item->link_image;
        }

        $cam_nang = Images::findAll(['type' => 'Cẩm nang']);
        $link_cam_nang = [];
        foreach ($cam_nang as $index => $item){
            $link_cam_nang[$index] = $item->link_image;
        }

        $data = ThongBao::find()->andFilterWhere(['type'=>'Bài đăng'])->all();
        foreach ($data as $item){
            $noi_dung = rtrim(str_replace(['&nbsp;'], '',strip_tags($item->noi_dung)));
            $item->noi_dung = $noi_dung;
        }

        return [
            'link_hinh_anh' => $link_image,
            'link_doi_tac' => $link_partner,
            'link_cam_nang' => $link_cam_nang,
            'noi_dung_bai_dang' => $data
        ];
    }

    public function actionDoiMatKhau(){
        $user = User::findOne(['id' => $_POST['uid']]);

        if (isset($_POST['newPass']) && isset($_POST['passwordOld'])){
            if ($_POST['newPass'] == '' && $_POST['passwordOld']==''){
                throw new HttpException(500, 'Vui lòng nhập thông tin');
            }
        }
        if (isset($_POST['passwordOld'])){
            if ($_POST['passwordOld'] == ''){
                throw new HttpException(500, 'Vui lòng nhập mật khẩu cũ');
            }
        }
        if (isset($_POST['newPass'])){
            if ($_POST['newPass'] == ''){
                throw new HttpException(500, 'Vui lòng nhập mật khẩu mới');
            }
        }

        if (\Yii::$app->getSecurity()->validatePassword($_POST['passwordOld'], $user->password_hash)){
            if(!\Yii::$app->getSecurity()->validatePassword($_POST['newPass'], $user->password_hash)){
                $new_pass = \Yii::$app->getSecurity()->generatePasswordHash($_POST['newPass']);
                $user->updateAttributes(['password_hash' => $new_pass]);

                return [
                    'message' => 'Đổi mật khẩu thành công'
                ];
            } else {
                throw new HttpException(500, 'Mật khẩu này đã được sử dụng, vui lòng thử lại');
            }

        } else {
            throw new HttpException(500, 'Mật khẩu cũ không đúng, vui lòng nhập lại');
        }
    }

    public function actionVoHieuHoaAccount(){
        $user = User::findOne($_POST['uid']);

        $user->updateAttributes(['status' => 0]);
        return [
            'message' => 'Vô hiệu hóa tài khoản thành công.'
        ];
    }

    public function actionGetDataNguoiDung(){
        $main_user = User::findOne([$_POST['uid']]);
        $old = [];
        $old['username'] = $main_user->username;
        $old['hoTen'] = $main_user->hoten;
        $old['phoneNum'] = $main_user->dien_thoai;
        $old['email'] = $main_user->email;

        $oldNgaySinh = explode('-', $main_user->ngay_sinh);
        $oldNgaySinh = array_reverse($oldNgaySinh);
        $oldNgaySinh = implode('/', $oldNgaySinh);
        $old['ngaySinh'] = $oldNgaySinh;

        return [
          'data' => $old
        ];
    }

    public function actionSuaThongTin(){
        $main_user = User::findOne([$_POST['uid']]);

        $user = User::findOne(['dien_thoai' => $_POST['phoneNum']]);
        if (!is_null($user)){
            if ($user->dien_thoai != $main_user->dien_thoai)
                throw new HttpException(500, 'Số điện thoại đã được sử dụng');
        }
        $user = User::findOne(['email' => $_POST['email']]);
        if (!is_null($user)){
            if ($user->email != $main_user->email)
                throw new HttpException(500, 'Email đã được sử dụng');
        }

        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            throw new HttpException(500, 'Email không hợp lệ, vui lòng nhập lại');
        }

        if (strlen($_POST['phoneNum']) != 10){
            throw new HttpException(500, 'Số điện thoại không hợp lệ, vui lòng nhập lại');
        }


        $main_user->updateAttributes(['hoten' => $_POST['hoTen']]);
        $main_user->updateAttributes(['email' => $_POST['email']]);
        $main_user->updateAttributes(['dien_thoai' => $_POST['phoneNum']]);

        $ngay_sinh = explode('/', $_POST['ngaySinh']);
        $ngay_sinh = array_reverse($ngay_sinh);
        $ngay_sinh = implode('-', $ngay_sinh);
        $main_user->updateAttributes(['ngay_sinh' => $ngay_sinh]);

        return [
            'message' => 'Sửa thông tin thành công'
        ];
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }
}
