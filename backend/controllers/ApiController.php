<?php


namespace backend\controllers;


use common\models\ThanSoHoc;
use common\models\User;
use SKAgarwal\GoogleApi\PlacesApi;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\web\Response;

class ApiController extends Controller
{
    //get-duong-doi
    public function actionGetDuongDoi(){
        return ThanSoHoc::tinhDuongDoi(User::findOne(1));
    }
    //get-su-menh
    public function actionGetSuMenh(){
        return ThanSoHoc::tinhSuMenh(User::findOne(1));
    }
    //get-chi-so-linh-hon
    public function actionGetChiSoLinhHon(){
        return ThanSoHoc::tinhChiSoLinhHon(User::findOne(1));
    }
    //get-chi-so-nhan-cach
    public function actionGetChiSoNhanCach(){
        return ThanSoHoc::tinhChiSoNhanCach(User::findOne(1));
    }
    //get-matrix
    public function actionGetMatrix(){
        return ThanSoHoc::getMatrixNumber(User::findOne(1));
    }
    //get-chi-so-thai-do
    public function actionGetChiSoThaiDo(){
        return ThanSoHoc::tinhChiSoThaiDo(User::findOne(1));
    }
    //get-chi-so-tuong-lai 031667696
    public function actionGetChiSoTuongLai(){
        return ThanSoHoc::tinhChiSoTuongLai(User::findOne(1));
    }
    //get-moc-cuoc-doi
    public function actionGetMocCuocDoi(){
        return ThanSoHoc::mocCuocDoi(User::findOne(1));
    }
    //tinh-so-luot-xem
    public function actionTinhSoLuotXem(){
        return ThanSoHoc::tinhSoLuotXem(User::findOne(407), 10000);
    }
    //tinh-can-chi
    public function actionTinhCanChi(){
        return ThanSoHoc::getCanChi(1992);
    }
    //get-menh
    public function actionGetMenh(){
        return ThanSoHoc::getMenh(1992);
    }
    //get-mau
    public function actionGetMau(){
        return ThanSoHoc::getMauSac(User::findOne(1));
    }
}
