<?php

namespace backend\controllers;

use common\models\myAPI;
use common\models\User;
use Yii;
use backend\models\DanhMuc;
use backend\models\search\DanhMucSearch;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * DanhMucController implements the CRUD actions for DanhMuc model.
 */
class DanhMucController extends CoreApiController
{
    public function behaviors()
    {

        $arr_action = ['get-all-khu-vuc', 'get-data', 'load', 'save', 'delete'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('DanhMuc', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    // get-all-khu-vuc
    public function actionGetAllKhuVuc(){
        $query = DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::KHU_VUC]);
        $data = [];
        foreach ($query as $item) {
            $data[] = ['label' => $item->name, 'value' => $item->id];
        }
        return [
            'khuVuc' => $data
        ];
    }

    // get-data
    public function actionGetData(){
        $query = DanhMuc::find();
        $totalCount = $query->count();
        $data = $query
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->andFilterWhere(['active' => 1])
            ->andFilterWhere(['type' => $this->dataPost])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }

    public function actionLoad(){
        $user = DanhMuc::find()->andFilterWhere(['id' => $this->dataPost['danh_muc']])
            ->select(['name', 'id'])
            ->one();
        if(!is_null($user))
            return $user;
        throw new HttpException(500, 'Không tìm thấy dữ liệu tương ứng');
    }

    public function actionSave(){
        if($this->dataPost['id'] == '')
            $model = new DanhMuc();
        else
            $model = DanhMuc::findOne($this->dataPost['id']);

        $model->name = $this->dataPost['name'];
        $model->type = $this->dataPost['type'];
        if($model->save())
            return [
                'content' => 'Đã cập nhật thông tin khu vực thành công'
            ];
        throw new HttpException(500, Html::errorSummary($model));
    }

    public function actionDelete(){
        $dataPost = myAPI::getDataPost();
        DanhMuc::updateAll(['active' => 0], ['id' => $dataPost['danh_muc']]);
        return [
            'message' => 'Đã xóa dữ liệu thành công',
        ];
    }
}
