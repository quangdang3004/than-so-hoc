<?php

namespace backend\controllers;

use common\models\myAPI;
use Yii;
use backend\models\ChucNang;
use backend\models\search\ChucNangSearch;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ChucNangController implements the CRUD actions for ChucNang model.
 */
class ChucNangController extends CoreApiController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $arr_action = ['get-data', 'save', 'load', 'delete', 'get-nhom-chuc-nang'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('ChucNang', $action_name, $uid);
                }
            ];
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' =>$rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionGetData(){
        $query = ChucNang::find();
        $totalCount = $query->count();
        $data = $query
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }

    public function actionSave(){
        if($this->dataPost['id'] == '')
            $model = new ChucNang();
        else
            $model = ChucNang::findOne($this->dataPost['id']);
        $model->name = $this->dataPost['name'];
        $model->nhom = $this->dataPost['nhom'];
        $model->controller_action = $this->dataPost['controller_action'];
        if($model->save())
            return [
                'content' => 'Đã lưu thông tin chức năng '.$model->name
            ];
        else
            throw new HttpException(500, Html::errorSummary($model));
    }

    public function actionLoad(){
        $model = ChucNang::findOne($this->dataPost['chuc_nang']);
        return [
            'result' => $model
        ];
    }

    public function actionDelete(){
        $model = ChucNang::findOne($this->dataPost['chuc_nang']);
        if($model->delete())
            return [
                'message' => 'Đã xóa chức năng '.$model->name.' thành công',
            ];
        else
            throw new HttpException(500, Html::errorSummary($model));
    }

    //get-nhom-chuc-nang
    public function actionGetNhomChucNang(){
        return [
            'optionsNhomChucNang' => (new ChucNang())->getNhomChucNang()
        ];
    }
}
