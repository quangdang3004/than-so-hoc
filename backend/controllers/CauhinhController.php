<?php

namespace backend\controllers;

use common\models\myAPI;
use Yii;
use backend\models\Cauhinh;
use backend\models\search\CauhinhSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * CauhinhController implements the CRUD actions for Cauhinh model.
 */
class CauhinhController extends CoreApiController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $arr_action = ['get-data', 'save'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
//                'matchCallback' => myAPI::isAccess2($controller, $item)
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('Cauhinh', $action_name, $uid);
                }
            ];
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' =>$rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cauhinh models.
     * @return mixed
     */
    public function actionGetData()
    {
        return [
            'namThanSo' => Cauhinh::findOne(['ghi_chu' => 'nam_than_so'])->content
        ];
    }

    /**
     * Displays a single Cauhinh model.
     * @param integer $id
     * @return mixed
     */
    public function actionSave()
    {
        if(isset($this->dataPost['namThanSo'])){
            if(trim($this->dataPost['namThanSo']) == '')
                throw new HttpException(500, 'Vui lòng nhập dữ liệu năm thần số');
            else{
                if(intval(str_replace(',','', $this->dataPost['namThanSo'])) < date("Y"))
                    throw new HttpException(500, 'Năm thần số tối thiểu là '.date("Y"));
                else{
                    Cauhinh::updateAll(['content' => intval(str_replace(',','', $this->dataPost['namThanSo']))], ['ghi_chu' => 'nam_than_so']);
                    return [
                        'content' => 'Đã lưu thông tin cấu hình thành công'
                    ];
                }
            }
        }
        else
            throw new HttpException(500, 'Không có thông tin cấu hình cần lưu');
    }

}
