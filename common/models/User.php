<?php
namespace common\models;
use app\models\QuanLyDaiLy;
use backend\models\ChiSo;
use backend\models\Cauhinh;
use backend\models\DanhMuc;
use backend\models\GiaoDich;
use backend\models\TrangThaiGiaoDich;
use backend\models\UserVaiTro;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use backend\models\YNghiaCacConSo;
use backend\models\YNghiaChiSo;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $input_tinh_bieu_do_so_ca_nhan
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $website
 * @property string|null $sercurity_code
 * @property string|null $auth_key
 * @property string|null $bieu_do_ti_le_so
 * @property string|null $thoi_gian_xem_xuat_file
 * @property int|null $status
 * @property int|null $phu_huynh_id
 * @property int|null $voucher_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $password
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $dia_chi
 * @property string|null $cmnd
 * @property string|null $anhdaidien
 * @property int|null $VIP
 * @property float|null $vi_dien_tu
 * @property int|null $dai_ly_id
 * @property string|null $ngay_sinh
 * @property string|null $code_ho_ten
 * @property int|null $so_lan_xem_con_lai
 * @property string|null $thong_tin_chi_so
 * @property string|null $gio_sinh
 * @property string|null $gio_sinh_str
 * @property string|null $nguon_tra_cuu
 * @property int|null $khu_vuc_id
 * @property int|null $goi_id
 * @property int|null $loi_nhuan_dai_ly
 * @property int|null $loi_nhuan
 * @property string|null $role_user
 * @property float|null $so_tien_can_thanh_toan
 * @property float|null $so_tien_da_thanh_toan
 * @property float|null $con_no
 * @property string|null $ma_dai_ly
 * @property string|null $ngay_thanh_toan_lan_dau
 * @property float|null $doanh_thu
 * @property float|null $phu_huynh
 * @property float|null $da_chi
 * @property float|null $so_tien
 * @property float|null $khuyen_mai
 *
 * @property ChiSo[] $chiSos
 * @property ChiSo[] $chiSos0
 * @property User $daiLy
 * @property GiaoDich[] $giaoDiches
 * @property GiaoDich[] $giaoDiches0
 * @property DanhMuc $khuVuc
 * @property TrangThaiGiaoDich[] $trangThaiGiaoDiches
 * @property User[] $users
 * @property Vaitrouser[] $vaitrousers
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $phu_huynh = null;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    public $files;
    public $khach_hang = false;
    public $dai_ly = false;
    public $thanh_vien = 0;
    public $phut_sinh;
    public $auth;
    public $uid;
    public $so_hai_con_so = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            TimestampBehavior::className(),
//        ];
//    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'VIP', 'dai_ly_id', 'so_lan_xem_con_lai', 'khu_vuc_id'], 'integer'],
            [['created_at', 'updated_at', 'ngay_sinh', 'gio_sinh', 'goi_id', 'bieu_do_ti_le_so', 'input_tinh_bieu_do_so_ca_nhan',
                'loi_nhuan', 'ngay_thanh_toan_lan_dau', 'loi_nhuan_dai_ly', 'thanh_vien', 'sercurity_code', 'website',
                'thoi_gian_xem_xuat_file', 'phu_huynh_id'], 'safe'],
            [['vi_dien_tu', 'so_tien_can_thanh_toan', 'so_tien_da_thanh_toan', 'con_no', 'doanh_thu', 'da_chi', 'so_tien', 'khuyen_mai'], 'number'],
            [['thong_tin_chi_so'], 'string'],
            [['username', 'password_hash', 'email', 'auth_key', 'password', 'hoten', 'anhdaidien', 'code_ho_ten', 'gio_sinh_str'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['dien_thoai', 'cmnd', 'ma_dai_ly'], 'string', 'max' => 20],
            [['dia_chi', 'nguon_tra_cuu'], 'string', 'max' => 200],
            [['role_user'], 'string', 'max' => 10],
            [['phuHuynh'], 'safe'],
            [['username'], 'unique'],
            [['dai_ly_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['dai_ly_id' => 'id']],
            [['khu_vuc_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['khu_vuc_id' => 'id']],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Tài khoản'),
            'password_hash' => Yii::t('app', 'Mật khẩu'),
            'status' => Yii::t('app', 'Trạng thái'),
            'hoten' => Yii::t('app', 'Họ tên'),
            'nhom' => Yii::t('app', 'Nhóm'),
            'VIP' => 'VIP',
            'vi_dien_tu' => 'Ví',
            'hoat_dong' => 'Hoạt động',
            'dia_chi' => 'Địa chỉ',
            'dien_thoai' => 'Điện thoại',
        ];
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /**
     * Gets query for [[ChiSos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiSos()
    {
        return $this->hasMany(ChiSo::className(), ['khach_hang_id' => 'id']);
    }
    /**
     * Gets query for [[ChiSos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiSos0()
    {
        return $this->hasMany(ChiSo::className(), ['user_id' => 'id']);
    }
    /**
     * Gets query for [[DaiLy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDaiLy()
    {
        return $this->hasOne(User::className(), ['id' => 'dai_ly_id']);
    }
    /**
     * Gets query for [[DoanhSoDauTuDaiLyThangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhSoDauTuDaiLyThangs()
    {
        return $this->hasMany(DoanhSoDauTuDaiLyThang::className(), ['user_id' => 'id']);
    }
    /**
     * Gets query for [[DoanhSoDauTuDaiLyThangs0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoanhSoDauTuDaiLyThangs0()
    {
        return $this->hasMany(DoanhSoDauTuDaiLyThang::className(), ['dai_ly_id' => 'id']);
    }
    /**
     * Gets query for [[GiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches()
    {
        return $this->hasMany(GiaoDich::className(), ['khach_hang_id' => 'id']);
    }
    /**
     * Gets query for [[GiaoDiches0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGiaoDiches0()
    {
        return $this->hasMany(GiaoDich::className(), ['user_id' => 'id']);
    }
    /**
     * Gets query for [[KhuVuc]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKhuVuc()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'khu_vuc_id']);
    }
    /**
     * Gets query for [[TrangThaiGiaoDiches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrangThaiGiaoDiches()
    {
        return $this->hasMany(TrangThaiGiaoDich::className(), ['user_id' => 'id']);
    }
    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['dai_ly_id' => 'id']);
    }
    /**
     * Gets query for [[Vaitrousers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVaitrousers()
    {
        return $this->hasMany(Vaitrouser::className(), ['user_id' => 'id']);
    }
    /**
     * @param $arrRoles
     * @return bool
     */
    public function isAccess($arrRoles){
        return !is_null(Vaitrouser::find()->andFilterWhere(['in', 'vaitro', $arrRoles])->andFilterWhere(['user_id' => Yii::$app->user->getId()])->one());
//        return 1;
    }
    public static function isViewAll($uid){
        return $uid == 1 || !is_null(UserVaiTro::findOne(['vai_tro' => VaiTro::QUAN_LY, 'id' => $uid]));
    }
    /**
     * @param $user \yii\web\User
     * @return string
     */
    public function getVaitros($user){
        $arr = [];
        foreach ($user->vaitrousers as $vaitrouser) {
            $arr[] = $vaitrouser->vaitro;
        }
        return implode(', ',$arr);
    }
    public function beforeDelete()
    {
        Vaitrouser::deleteAll(['user_id' => $this->id]);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
    /**
     * @param $type string
     * @param $value string
     * @param $user User
     * @param $luanGiai array|YNghiaCacConSo[]|\yii\db\ActiveRecord[]
     */
    public function luuChiSo($type, $value, $user, $luanGiai, $thutu = 0, $so_hai_con_so = null, $strSoDuongDoi = null){
        $model = new ChiSo();
        $model->loai_chi_so = $type;
        $model->noi_dung = strval($value);
        $model->khach_hang_id = $user->id;
        $model->thu_tu = $thutu;
        if(!is_null($so_hai_con_so))
            $model->so_hai_con_so = $so_hai_con_so;
        $dataPost = myAPI::getDataPost();
        if(isset($dataPost['uid']))
            $model->user_id = $dataPost['uid'];
        if(!is_null($strSoDuongDoi))
            $model->str_so_duong_doi = $strSoDuongDoi;
        if(!$model->save()){
            myAPI::sendMailAmzon(Html::errorSummary($model), 'hungddvimaru@gmail.com', ['hungddvimaru@gmail.com'], 'error');
            throw new HttpException(500, Html::errorSummary($model));
        }
        else{
            foreach ($luanGiai as $item) {
                $modelYNghiaChiSo = new YNghiaChiSo();
                $modelYNghiaChiSo->y_nghia_id = $item->id;
                $modelYNghiaChiSo->chi_so_id = $model->id;
                $modelYNghiaChiSo->save();
            }
        }
        return true;
    }
    public function beforeSave($insert)
    {
        $dataPost = myAPI::getDataPost();
        $this->hoten = trim($this->hoten);
        $this->dien_thoai = trim($this->dien_thoai);
        if($insert){
            if(!isset($_POST['submitted'])){
                if($this->thanh_vien != 1){
                    if(empty($this->dai_ly_id))
                        $this->dai_ly_id = $dataPost['uid'];
                }
            }
            else{
                if($this->thanh_vien != 1)
                    $this->dai_ly_id = 1; // admin
            }
            $this->created_at = date("Y-m-d H:i:s");
        }
        else{
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
    public function afterSave($insert, $changedAttributes)
    {
        $chiSo = ChiSo::findAll(['khach_hang_id' => $this->id]);
        foreach ($chiSo as $item) {
            $item->delete();
        }
        if($insert){
            if($this->khach_hang == 1 || $this->dai_ly == 1 || isset($_POST['submitted'])){
                if($this->khach_hang == 1 || isset($_POST['submitted'])){
                    $vaiTroKhachHang = VaiTro::findOne(['name' => VaiTro::KHACH_HANG]);
//                    if(!User::isViewAll()){
//                        //Trừ 1 lần xem của đại lý
//                        $daiLy = User::findOne($this->dai_ly_id);
//                        if($daiLy->username != 'khachonline')
//                            $daiLy->updateAttributes(['so_lan_xem_con_lai' => $daiLy->so_lan_xem_con_lai - 1]);
//                    }
                }else if($this->dai_ly == 1){
                    $vaiTroKhachHang = VaiTro::findOne(['name' => VaiTro::DAI_LY]);
                }
                $vaitronguoidung = new Vaitrouser();
                $vaitronguoidung->user_id = $this->id;
                $vaitronguoidung->vaitro_id = $vaiTroKhachHang->id;
                $vaitronguoidung->save();
            }
        }
        if($this->khach_hang == 1 || isset($_POST['submitted'])){
            // 1 - Số Đường Đời
            $data = ThanSoHoc::tinhDuongDoi($this);
            $soDuongDoi = $data['duong_doi'];
            $tuoiVongDoi = $data['tuoi_vong_doi'];
            $luanGiai = ThanSoHoc::getLuanGiai($data['duong_doi'], 'Đường đời');
            $this->luuChiSo(ChiSo::DUONG_DOI, $data['duong_doi'], $this, $luanGiai, 1, $data['soTruoc'], $data['strSoDuongDoi']);

            $luanGiaiTreEm = ThanSoHoc::getLuanGiai($data['duong_doi'], ChiSo::DUONG_DOI_TRE_EM);
            $this->luuChiSo(ChiSo::DUONG_DOI_TRE_EM, $data['duong_doi'], $this, $luanGiaiTreEm, 1, $data['soTruoc'], $data['strSoDuongDoi']);

            $luanGiaiChaMe = ThanSoHoc::getLuanGiai($data['duong_doi'], ChiSo::DUONG_DOI_CHA_ME);
            $this->luuChiSo(ChiSo::DUONG_DOI_CHA_ME, $data['duong_doi'], $this, $luanGiaiChaMe, 1, $data['soTruoc'], $data['strSoDuongDoi']);

            // 9 - Số vận mệnh
            $data = ThanSoHoc::tinhSuMenh($this);
            $luanGiai = ThanSoHoc::getLuanGiai($data['tong'], 'Chỉ số vận mệnh');
            $this->luuChiSo(ChiSo::SO_VAN_MENH, $data['tong'], $this, $luanGiai, 9, $data['soTruoc'], $data['strSoVanMenh']);

            $luanGiaiTreEm = ThanSoHoc::getLuanGiai($data['tong'], ChiSo::VAN_MENH_TRE_EM);
            $this->luuChiSo(ChiSo::VAN_MENH_TRE_EM, $data['tong'], $this, $luanGiaiTreEm, 9, $data['soTruoc'], $data['strSoVanMenh']);
            $soVanMenh = $data['tong'];

//            // 2 - Số Ngày sinh
            $luanGiai = ThanSoHoc::getLuanGiai(date("j", strtotime($this->ngay_sinh)), 'Chỉ số ngày sinh');
            $this->luuChiSo(ChiSo::CHI_SO_NGAY_SINH, date("j", strtotime($this->ngay_sinh)), $this, $luanGiai, 2); // từ 1 đến 31

            $luanGiaiTreEm = ThanSoHoc::getLuanGiai(date("j", strtotime($this->ngay_sinh)), ChiSo::NGAY_SINH_TRE_EM);
            $this->luuChiSo(ChiSo::NGAY_SINH_TRE_EM, date("j", strtotime($this->ngay_sinh)), $this, $luanGiaiTreEm, 2); // từ 1 đến 31
//
//            // 3 - Vòng đời
            $data = ThanSoHoc::tinhChiSoVongDoi($this);
            $luanGiai = ThanSoHoc::getLuanGiai($data, 'Chỉ số vòng đời');
            $this->luuChiSo(ChiSo::VONG_DOI, implode(' - ', [
                'VÒNG ĐỜI 1: '.$data['VD1'],
                'VÒNG ĐỜI 2: '.$data['VD2'],
                'VÒNG ĐỜI 3: '.$data['VD3'],
            ]), $this,$luanGiai, 3);
            // 4 Tuổi vòng đời
            $this->luuChiSo(ChiSo::TUOI_CUA_VONG_DOI, $tuoiVongDoi, $this, [], 4);

            // 5 Đỉnh trưởng thành
            $data = ThanSoHoc::tinhDinhTruongThanh($this, $soDuongDoi);
            $luanGiai = ThanSoHoc::getLuanGiai($data['arr_dinh'], 'Đỉnh trưởng thành');
            $this->luuChiSo(ChiSo::DINH_TRUONG_THANH, $data['dinh'], $this, $luanGiai, 5);

            $soTruongThanh = $data['arr_dinh'];
            // 6 - Tuổi đỉnh trưởng thành
            $this->luuChiSo(ChiSo::TUOI_DINH_TRUONG_THANH, $data['tuoiDinh'], $this, [], 6);
            $thongTinChiSo[] = ChiSo::TUOI_DINH_TRUONG_THANH.': '.$data['tuoiDinh'];

            // 7 - Số thử thách
            $data = ThanSoHoc::tinhSoThuThach($this);
            $luanGiai = ThanSoHoc::getLuanGiai($data['arr_so'],'Số thách thức');
            $this->luuChiSo(ChiSo::SO_THU_THACH, $data['thuThach'], $this, $luanGiai, 7);

            $luanGiaiTreEm = ThanSoHoc::getLuanGiai($data['arr_so'],ChiSo::THU_THACH_TRE_EM);
            $this->luuChiSo(ChiSo::THU_THACH_TRE_EM, $data['thuThach'], $this, $luanGiaiTreEm, 7);

            $luanGiaiChaMe = ThanSoHoc::getLuanGiai($data['arr_so'], ChiSo::THU_THACH_CHA_ME);
            $this->luuChiSo(ChiSo::THU_THACH_CHA_ME, $data['thuThach'],  $this, $luanGiaiChaMe, 7);

            // 8 - Thời gian thử thách
            $this->luuChiSo(ChiSo::THOI_GIAN_THU_THACH, $data['thoiGianThuThach'], $this, [], 8);
            // 10 - Số thể hiện
            $data = ThanSoHoc::tinhSoTheHien($this);
            $this->luuChiSo(ChiSo::SO_THE_HIEN, $data, $this, [], 10);
            $soTheHien = $data;

            // 11 - Số tâm hồn
            $data = ThanSoHoc::tinhSoTamHon($this);
            $luanGiai = ThanSoHoc::getLuanGiai($data, 'Chỉ số tâm hồn');
            $this->luuChiSo(ChiSo::SO_TAM_HON, $data, $this, $luanGiai, 11);

            $luanGiaiTreEm = ThanSoHoc::getLuanGiai($data, ChiSo::TAM_HON_TRE_EM);
            $this->luuChiSo(ChiSo::TAM_HON_TRE_EM, $data, $this, $luanGiaiTreEm, 11);
            $soTamHon = $data;

            // 12 - Số bản thể
            $data = ThanSoHoc::tinhChiSoBanThe($this);
            $luanGiai = ThanSoHoc::getLuanGiai($data, ChiSo::SO_BAN_THE);
            $this->luuChiSo(ChiSo::SO_BAN_THE, $data, $this, $luanGiai, 12);
            $soBanThe = $data;
            // 13 - Số lý trí
            $data = ThanSoHoc::tinhSoLyTri($this);
            $luanGiai = ThanSoHoc::getLuanGiai($data, ChiSo::SO_LY_TRI);
            $this->luuChiSo(ChiSo::SO_LY_TRI, $data, $this, $luanGiai, 13);
            $soLyTri = $data;
            // 14 - Năm thần số
            $data = ThanSoHoc::tinhNamThanSo($this);
            $namThanSo = $data;
            $luanGiai = ThanSoHoc::getLuanGiai($data, 'Năm thần số');
            $this->luuChiSo(ChiSo::NAM_THAN_SO, $data, $this, $luanGiai, 14);
//
            // 15 - Tháng thần số
            $data = ThanSoHoc::tinhThangThanSo($namThanSo);
            $luanGiai = ThanSoHoc::getLuanGiai($data, 'Tháng thần số');
            $this->luuChiSo(ChiSo::THANG_THAN_SO, $data, $this, $luanGiai, 15);
//
            // 18 - Số trưởng thành
            $data = ThanSoHoc::tinhSoTruongThanh($soDuongDoi, $soVanMenh);
            if($data == 33)
                $data = 6;
            $soTruongThanh = $data;
            $luanGiai = ThanSoHoc::getLuanGiai($data, ChiSo::SO_TRUONG_THANH);
            $this->luuChiSo(ChiSo::SO_TRUONG_THANH, $data, $this, $luanGiai, 18);

            $luanGiaiTreEm = ThanSoHoc::getLuanGiai($data, ChiSo::TRUONG_THANH_TRE_EM);
            $this->luuChiSo(ChiSo::TRUONG_THANH_TRE_EM, $data, $this, $luanGiaiTreEm, 18);

            $luanGiaiChaMe = ThanSoHoc::getLuanGiai($data, ChiSo::TRUONG_THANH_CHA_ME);
            $this->luuChiSo(ChiSo::TRUONG_THANH_CHA_ME, $data, $this, $luanGiaiChaMe, 18);

            // 16 - Biểu đồ số cá nhân
            $data = ThanSoHoc::bieuDoSoCaNhan($this, $soDuongDoi, $soVanMenh, $soTruongThanh, $soBanThe, $soLyTri, $soTamHon);
            $this->updateAttributes(['input_tinh_bieu_do_so_ca_nhan' => implode(',', [$soDuongDoi, $soVanMenh, $soTruongThanh, $soBanThe, $soLyTri, $soTamHon])]);
//            $table = "<table class='table table-bordered table-striped'><tr><th>Số</th>{{dong1}}</tr><tr><th>Tỉ lệ</th>{{dong2}}</tr><tr><th>Số</th>{{dong3}}</tr><tr><th>Tỉ lệ (%)</th>{{dong4}}</tr></table>";
//            $dong1 = '<th>0</th><th>1</th><th>2</th><th>3</th><th>4</th>';
//            $dong2=  "<td>{$data[0]}%</td><td>{$data[1]}%</td><td>{$data[2]}%</td><td>{$data[3]}%</td><td>{$data[4]}%</td>";
//            $dong3 = '<th>5</th><th>6</th><th>7</th><th>8</th><th>9</th>';
//            $dong4 = "<td>{$data[5]}%</td><td>{$data[6]}%</td><td>{$data[7]}%</td><td>{$data[8]}%</td><td>{$data[9]}%</td>";
//            $table = str_replace('{{dong1}}', $dong1, $table);
//            $table = str_replace('{{dong2}}', $dong2, $table);
//            $table = str_replace('{{dong3}}', $dong3, $table);
//            $table = str_replace('{{dong4}}', $dong4, $table);
            $this->luuChiSo(ChiSo::BIEU_DO_SO_CA_NHAN, $data, $this, [], 16);
            // 17 - Sơ đồ ngày sinh và mũi tên cá tính
            $data = ThanSoHoc::bieuDoNgaySinh($this);
            $this->luuChiSo(ChiSo::SO_DO_NGAY_SINH, $data, $this, [], 17);

            // Tính chỉ số thử thách thói quen
            $data = ThanSoHocV2::tinhSoThuThachThoiQuen($this);
            $luanGiai = ThanSoHoc::getLuanGiai($data, YNghiaCacConSo::SO_THU_THACH_THOI_QUEN);
            $this->luuChiSo(YNghiaCacConSo::SO_THU_THACH_THOI_QUEN, $data, $this, $luanGiai, 17);
        }
        if($this->khach_hang != 1 && $this->dai_ly != 1 && !isset($_POST['submitted'])){
            $vaitro = Vaitrouser::findAll(['user_id' => $this->id]);
            $dataPost = myAPI::getDataPost();
            foreach ($vaitro as $item) {
                $item->delete();
            }
            foreach ($dataPost['vai_tro'] as $item) {
                $vaitronguoidung = new Vaitrouser();
                $vaitronguoidung->vaitro_id = $item;
                $vaitronguoidung->user_id = $this->id;
                $vaitronguoidung->save();
            }
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
    public function getDaiLyTree($idParent, &$arrDaiLyTree = []){
        $nhomcon = QuanLyDaiLy::find()
            ->andFilterWhere(['dai_ly_id' => $idParent])
            ->all();
        /** @var QuanLyDaiLy $item */
        foreach ($nhomcon as $item) {
            self::getDaiLyTree($item->id, $arrDaiLyTree[$item->id]['children']);
            $arrDaiLyTree[] = [
                'hoten' => $item->hoten,
                'dien_thoai' => $item->dien_thoai,
                'children' => []
            ];
        }
    }
    public function duyetNhom($parentid = 0, $trees = NULL){
        if(!$trees) $trees = array();
        $nhoms = User::find()->select(['hoten', 'id', 'dien_thoai'])
            ->andWhere('ma_dai_ly is not null')
            ->andFilterWhere(['dai_ly_id' => $parentid])->all();
        /** @var  $nhom  User*/
        foreach ($nhoms as $index => $nhom) {
            $newNodes = array('name'=>$nhom->hoten, 'value' => $nhom->id);
            $newNodes['children'] = $this->duyetNhom($nhom->id);
            $trees[] = $newNodes;
        }
        return $trees;
    }
    public function dsNhom($dai_ly_id){
        ini_set('memory_limit', -1);
        $model = User::find()->select(['hoten', 'id', 'dien_thoai'])->andFilterWhere(['id' => $dai_ly_id])->one();
        $child_trees = array();
        $trees = array('name'=>$model->hoten, 'value' => $model->id,
            'children' => $this->duyetNhom($model->id, $child_trees)
        );
        return $trees;
    }
}
