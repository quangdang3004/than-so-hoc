<?php
namespace common\models;
use backend\models\Cauhinh;
use backend\models\ChiSo;
use backend\models\GiaoDich;
use backend\models\YNghiaCacConSo;
use backend\models\YNghiaChiSo;
use Mpdf\Tag\Th;
use yii\helpers\VarDumper;
class ThanSoHoc
{
    /**
     * @param $tong
     * @return array
     */
    function tinhTong($tong){
        $soTruoc = $tong;
        if($tong >= 10 && $tong != 11 && $tong != 22 && $tong != 33){
            while ($tong >= 10  && $tong != 11 && $tong != 22 && $tong != 33){
                $strTong = strval($tong);
                if($strTong != 10)
                    $soTruoc = $strTong;
                $tong = 0;
                for($i = 0; $i<strlen($strTong); $i++){
                    $tong += intval($strTong[$i]);
                }
            }
        }
        return  [
            'tong' => $tong,
            'soTruoc' => $soTruoc
        ];
    }
    /**
     * @param $user User
     * @param $typeCauHinh string //<p>thoi_gian_cho_phep_sua, thoi_gian_cho_phep_xoa</p>
     * @return bool
     *
     */
    public function isAllowEditOrDelete($user, $typeCauHinh){
        if(User::isViewAll())
            return  true;
        else{
            if($user->dai_ly_id == \Yii::$app->user->id){
                $thoiGianChoPhepSua = intval(Cauhinh::findOne(['ghi_chu' => $typeCauHinh])->content);
                $thoiGianChoPhep = date("Y-m-d H:i:s", strtotime("-{$thoiGianChoPhepSua} minutes", time()));
                return $thoiGianChoPhep <= $user->created_at;
            }else
                return  false;
        }
    }
    public static function getTenChuongTrinh(){
        return Cauhinh::findOne(['ghi_chu' => 'ten_chuong_trinh'])->content;
    }
    /**
     * @param $user User
     * @return bool
     */
    public static function isAllowEdit($user){
        return (new ThanSoHoc)->isAllowEditOrDelete($user, 'thoi_gian_cho_phep_sua');
    }
    /**
     * @param $user User
     * @return bool
     */
    public static function isAllowDelete($user){
        return (new ThanSoHoc)->isAllowEditOrDelete($user, 'thoi_gian_cho_phep_xoa');
    }
    /**
     * @param $user User
     *
     */
    public static function tinhDuongDoi($user){
        $soPhuHop = [
            1 => '1, 5, 7',
            2 => '2, 4, 8',
            3 => '3, 6, 9',
            4 => '2, 4, 8',
            5 => '1, 5, 7',
            6 => '3, 6, 9',
            7 => '1, 5, 7',
            8 => '2, 4, 8',
            9 => '3, 6, 9',
            11 => '',
            22 => '',
            33 => '',
        ];
        $soKhac = [
            1 => '2, 4, 8',
            2 => '5, 7',
            3 => '4, 7, 8',
            4 => '1, 3, 5, 9',
            5 => '2, 4, 6',
            6 => '1, 5, 7',
            7 => '2, 3, 6, 8, 9',
            8 => '1, 3, 7, 9',
            9 => '4, 7, 8',
            11 => '',
            22 => '',
            33 => ''
        ];
        $soHoanHao = [
            1 => '3, 5, 6',
            2 => '6, 8, 9',
            3 => '1, 5, 7',
            4 => '1, 7, 8',
            5 => '1, 3, 7',
            6 => '1, 2, 8, 9',
            7 => '3, 5',
            8 => '2, 4, 6',
            9 => '2, 6',
            11 => '',
            22 => '',
            33 => ''
        ];
        $tuoiVongDoi = [
            1 => 'VÒNG ĐỜI 1: TỪ 0-26 TUỔI - VÒNG ĐỜI 2: TỪ 27-53 TUỔI - VÒNG ĐỜI 3: > 53',
            2 => 'VÒNG ĐỜI 1: TỪ 0-25 TUỔI - VÒNG ĐỜI 2: TỪ 26-52 TUỔI - VÒNG ĐỜI 3: > 52',
            3 => 'VÒNG ĐỜI 1: TỪ 0-33 TUỔI - VÒNG ĐỜI 2: TỪ 34-60 TUỔI - VÒNG ĐỜI 3: > 60',
            4 => 'VÒNG ĐỜI 1: TỪ 0-32 TUỔI - VÒNG ĐỜI 2: TỪ 33-59 TUỔI - VÒNG ĐỜI 3: > 59',
            5 => 'VÒNG ĐỜI 1: TỪ 0-31 TUỔI - VÒNG ĐỜI 2: TỪ 32-58 TUỔI - VÒNG ĐỜI 3: > 58',
            6 => 'VÒNG ĐỜI 1: TỪ 0-30 TUỔI - VÒNG ĐỜI 2: TỪ 31-57 TUỔI - VÒNG ĐỜI 3: > 57',
            7 => 'VÒNG ĐỜI 1: TỪ 0-29 TUỔI - VÒNG ĐỜI 2: TỪ 30-56 TUỔI - VÒNG ĐỜI 3: > 56',
            8 => 'VÒNG ĐỜI 1: TỪ 0-28 TUỔI - VÒNG ĐỜI 2: TỪ 29-55 TUỔI - VÒNG ĐỜI 3: > 55',
            9 => 'VÒNG ĐỜI 1: TỪ 0-27 TUỔI - VÒNG ĐỜI 2: TỪ 28-54 TUỔI - VÒNG ĐỜI 3: > 54',
            11 => 'VÒNG ĐỜI 1: TỪ 0-25 TUỔI - VÒNG ĐỜI 2: TỪ 26-52 TUỔI - VÒNG ĐỜI 3: > 52',
            22 => 'VÒNG ĐỜI 1: TỪ 0-32 TUỔI - VÒNG ĐỜI 2: TỪ 33-59 TUỔI - VÒNG ĐỜI 3: > 59',
            33 => 'VÒNG ĐỜI 1: TỪ 0-30 TUỔI - VÒNG ĐỜI 2: TỪ 31-57 TUỔI - VÒNG ĐỜI 3: > 57',
        ];
        $ngaySinh = explode('-', $user->ngay_sinh); //str_replace('-', '', $user->ngay_sinh);
        // Cách 1: Tổng từng phần tử: [Tổng ngày] + [Tổng tháng] + [Tổng năm]
        $tong = (new ThanSoHoc)->tinhTong($ngaySinh[0])['tong'] + (new ThanSoHoc)->tinhTong($ngaySinh[1])['tong'] + (new ThanSoHoc)->tinhTong($ngaySinh[2])['tong'];
        $result = (new ThanSoHoc)->tinhTong($tong);
        $tong = $result['tong'];
        $soTruoc = $result['soTruoc'];

        // Cách 2: Tính tổng tất cả các con số
        $strNgaySinh = str_replace('-', '', $user->ngay_sinh);
        $tong2 = 0;
        for ($i = 0; $i<strlen($strNgaySinh); $i++) {
            $tong2 += intval($strNgaySinh[$i]);
        }
        $result = (new ThanSoHoc)->tinhTong($tong2);
        $tong2 = $result['tong'];
        $soTruoc2 = $result['soTruoc'];

        // Cách 3: Cộng dọc
        $tong3 = intval($ngaySinh[0]) + intval($ngaySinh[1]) + intval($ngaySinh[2]);
        $result = (new ThanSoHoc)->tinhTong($tong3);
        $tong3 = $result['tong'];
        $soTruoc3 = $result['soTruoc'];

        $strSoDuongDoi = [$soTruoc => $soTruoc];

        if(in_array($soTruoc2, [11, 22, 33])){
            $tong = $tong2;
            $soTruoc = $soTruoc2;
            if(!in_array($soTruoc2, $strSoDuongDoi))
                $strSoDuongDoi[$soTruoc2] = $soTruoc2;
        }else if(in_array($soTruoc3, [11, 22, 33])){
            $tong = $tong3;
            $soTruoc = $soTruoc3;
            if(!in_array($soTruoc3, $strSoDuongDoi))
                $strSoDuongDoi[$strSoDuongDoi] = $soTruoc3;
        }

        if($tong == 11)
            $strSoDuongDoi[] = 2;
        else if($tong == 22)
            $strSoDuongDoi[] = 4;
        else if($tong == 33)
            $strSoDuongDoi[] = 6;
        return  [
            'duong_doi' => $tong,
            'con_so_phu_hop' => $soPhuHop[$tong],
            'so_khac' => $soKhac[$tong],
            'ban_doi_hoan_hao' => $soHoanHao[$tong],
            'tuoi_vong_doi' => $tuoiVongDoi[$tong],
            'soTruoc' => $soTruoc,
            'strSoDuongDoi' => implode('/', $strSoDuongDoi)
        ];
    }
    /**
     * @param $user User
     */
    public static function tinhSuMenh($user){
        $arr_str = [
            'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5, 'f' => 6, 'g' => 7, 'h' => 8, 'i' => 9,
            'j' => 1, 'k' => 2, 'l' => 3, 'm' => 4, 'n' => 5, 'o' => 6, 'p' => 7, 'q' => 8, 'r' => 9,
            's' => 1, 't' => 2, 'u' => 3, 'v' => 4, 'w' => 5, 'x' => 6, 'y' => 7, 'z' => 8
        ];
        // Cách 1: Số sứ mệnh = tổng các chữ cái trong họ tên
        $code_ho_ten = mb_strtolower(str_replace('-', '', trim(myAPI::createCode($user->hoten)))) ;
        $tong = 0;
        for ($i = 0 ; $i< strlen($code_ho_ten); $i++) {
            if(isset($arr_str[$code_ho_ten[$i]]))
                $tong += $arr_str[$code_ho_ten[$i]];
        }
        $tong1 = (new ThanSoHoc)->tinhTong($tong);
        $soTruoc1 = $tong1['soTruoc'];

        if($tong1['soTruoc'] > 10 && $tong1['soTruoc'] != 11 && $tong1['soTruoc'] != 22){
            $tongSau = (new ThanSoHoc)->tinhTong($tong1['soTruoc'])['tong'];
            if($tongSau > 10)
                $soTruoc1 = $tongSau;
        }

        // Cách 2: Số sứ mệnh = Tổng (Tổng họ + Tổng đệm + Tổng tên)
        $tongSoSuMenh = 0;
        $Arr_code_ho_ten = array_filter(explode('-', mb_strtolower(trim(myAPI::createCode($user->hoten)))) ) ;
        foreach ($Arr_code_ho_ten as $item) {
            $tong = 0;
            for ($i = 0 ; $i< strlen($item); $i++) {
                if(isset($arr_str[$item[$i]]))
                    $tong += $arr_str[$item[$i]];
            }
            $tong = (new ThanSoHoc)->tinhTong($tong);
            if($tong['tong'] == 33)
                $tongSoSuMenh += 6;
            else
                $tongSoSuMenh += $tong['tong'];
        }
        $tong2 = (new ThanSoHoc)->tinhTong($tongSoSuMenh);
        $soTruoc2 = $tong2['soTruoc'];

        if($tong2['soTruoc'] > 10 && $tong2['soTruoc'] != 11 && $tong2['soTruoc'] != 22){
            $tongSau = (new ThanSoHoc)->tinhTong($tong2['soTruoc'])['tong'];
            if($tongSau > 10)
                $soTruoc2 = $tongSau;
        }

        // xử lý phân tách các số
        $strSoVanMenh = [$soTruoc1 => $soTruoc1, $soTruoc2 => $soTruoc2];
        if($tong1['tong'] == 33)
            $tong1 = 6;
        else
            $tong1 = $tong1['tong'];

        if($tong2['tong'] == 33)
            $tong2 = 6;
        else
            $tong2 = $tong2['tong'];

        $strSoVanMenh[$tong1] = $tong1;
        if($tong2 != $tong1)
            $strSoVanMenh[$tong2] = $tong2;
//
//        if($tong['tong'] == 33)
//            $tong = 6;
//
//        $tongSoSuMenh += ;
//        $tong = (new ThanSoHoc)->tinhTong($tongSoSuMenh);
//        return [
//            'tong' => $tong,
//            'soTruoc' => 33
//        ];

//        if($tong['tong'] == 33)
//            return [
//                'tong' => 6,
//                'soTruoc' => 33
//            ];
//        else return $tong;

        return [
            'tong' => $tong1,
            'soTruoc' => $soTruoc1,
            'strSoVanMenh' => implode('/', $strSoVanMenh)
        ];
    }

    /**
     * @param $user User
     */
    public static function tinhChiSoLinhHon($user){
        $arr_phu_am = [
            'a' => 1,
            'e' => 5,
            'u' => 3,
            'i' => 9,
            'o' => 6,
            'w' => 5
        ];
        $arr_nguyen_am_hoten = [];
        $code_ho_ten = mb_strtolower(str_replace('-', '', trim(myAPI::createCode($user->hoten))));
        $tong = 0;
        for ($i=0; $i<strlen($code_ho_ten); $i++){
            $tong += (isset($arr_phu_am[$code_ho_ten[$i]]) ? intval($arr_phu_am[$code_ho_ten[$i]]) : 0);
//            echo isset($arr_phu_am[$code_ho_ten[$i]]) ? intval($arr_phu_am[$code_ho_ten[$i]]) : 0;;
            if(isset($arr_phu_am[$code_ho_ten[$i]]))
                $arr_nguyen_am_hoten[] = $code_ho_ten[$i];
        }
        $tong = (new ThanSoHoc)->tinhTong($tong)['tong'];
        return [
            'tong' => $tong,
            'phu_am' => implode(', ',$arr_nguyen_am_hoten)
        ];
    }
    /**
     * @param $user User
     */
    public static function tinhChiSoBanThe($user){
        $arr_str = [
            'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5, 'f' => 6, 'g' => 7, 'h' => 8, 'i' => 9,
            'j' => 1, 'k' => 2, 'l' => 3, 'm' => 4, 'n' => 5, 'o' => 6, 'p' => 7, 'q' => 8, 'r' => 9,
            's' => 1, 't' => 2, 'u' => 3, 'v' => 4, 'w' => 5, 'x' => 6, 'y' => 7, 'z' => 8
        ];
        $arrChuCaiLapLai = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0];
        $code_ho_ten = mb_strtolower(str_replace('-', '', trim(myAPI::createCode($user->hoten))));
        for ($i=0; $i<strlen($code_ho_ten); $i++){
            if(isset($code_ho_ten[$i]))
                if(isset($arr_str[$code_ho_ten[$i]]))
                    $arrChuCaiLapLai[$arr_str[$code_ho_ten[$i]]]++;
        }
        $arrChuCaiLapLai = array_filter($arrChuCaiLapLai);
        return count($arrChuCaiLapLai);
    }
    /**
     * @param $user User
     */
    public static function tinhSoTheHien($user){
        $arr_str = [
            'b' => 2, 'c' => 3, 'd' => 4, 'f' => 6, 'g' => 7, 'h' => 8,
            'j' => 1, 'k' => 2, 'l' => 3, 'm' => 4, 'n' => 5, 'p' => 7, 'q' => 8, 'r' => 9,
            's' => 1, 't' => 2, 'v' => 4, 'w' => 5, 'x' => 6, 'z' => 8
        ];
        $code_ho_ten = mb_strtolower(str_replace('-', '', trim(myAPI::createCode($user->hoten))));
        $tong = 0;
        for ($i = 0 ; $i< strlen($code_ho_ten); $i++) {
            if(isset($arr_str[$code_ho_ten[$i]]))
                $tong += $arr_str[$code_ho_ten[$i]];
        }
        $tong = (new ThanSoHoc())->tinhTong($tong)['tong'];
        if($tong == 33)
            $tong = 6;
        return $tong;
    }
    /**
     * @param $user User
     * @return int
     */
    public static function tinhSoTamHon($user){
        $arr_str = [
            'a' => 1, 'e' => 5, 'i' => 9, 'o' => 6, 'u' => 3, 'y' => 7
        ];
        $code_ho_ten = mb_strtolower(str_replace('-', '', trim(myAPI::createCode($user->hoten))));
        $tong = 0;
        for ($i = 0 ; $i< strlen($code_ho_ten); $i++) {
            if(isset($arr_str[$code_ho_ten[$i]]))
                $tong += $arr_str[$code_ho_ten[$i]];
        }
        $tong = (new ThanSoHoc())->tinhTong($tong)['tong'];
        if($tong == 33)
            $tong = 6;
        return $tong;
    }
    /**
     * @param $user User
     */
    public static function getMatrixNumber($user){
        $arr_number = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0];
        $strArr = explode(',', 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z');
        $intArr = [1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8];
        for($i = 0; $i<strlen($user->ngay_sinh); $i++)
            if(isset($arr_number[intval($user->ngay_sinh[$i])]))
                $arr_number[intval($user->ngay_sinh[$i])]++;
        $code_hoten = mb_strtolower(str_replace('-', '', myAPI::createCode($user->hoten)));
        for($i = 0; $i<strlen($code_hoten); $i++)
            if(in_array($code_hoten[$i], $strArr)){
                $arr_number[$intArr[array_search($code_hoten[$i], $strArr)]]++;
            }
        return $arr_number;
    }
    /**
     * @param $user User
     * @return string
     */
    public static function tinhSoThieu($user){
        $matrix = self::getMatrixNumber($user);
        $soThieu = [];
        foreach ($matrix as $item => $value) {
            if($value == 0)
                $soThieu[] = $item;
        }
        if(count($soThieu) > 0)
            return implode(', ', $soThieu);
        return  'Không có';
    }
    /**
     * @param $user User
     */
    public static function tinhSoLapLai($user){
        $matrix = self::getMatrixNumber($user);
        $soLap = [];
        foreach ($matrix as $item => $value) {
            if($value > 1)
                $soLap[] = $item.' ('.$value.' lần)';
        }
        if(count($soLap) > 0)
            return implode(', ', $soLap);
        return  'Không có';
    }
    /**
     * @param $user User
     */
    public static function tinhChiSoThaiDo($user){
        $ngay_thang_sinh = date("dm", strtotime($user->ngay_sinh));
        $tong = intval($ngay_thang_sinh);
        $tong = (new ThanSoHoc())->tinhTong($tong)['tong'];
        return $tong;
    }
    /**
     * @param $user User
     */
    public static function tinhChiSoTuongLai($user){
        $tong = 0;
        for($i = 0; $i<strlen($user->cmnd); $i++){
            $tong += intval($user->cmnd[$i]);
        }
        $tong = (new ThanSoHoc())->tinhTong($tong)['tong'];
        return  $tong;
    }
    /** [chi so thang sinh]  --------- [chi so ngay sinh] ---------- [chi so nam sinh]*/
    /**
     * @param $user User
     */
    public static function mocCuocDoi($user){
        $chiSoNgaySinh = self::tinhChiSoNgaySinh($user);
        $thangSinh = (new ThanSoHoc())->tinhTong(date("m", strtotime($user->ngay_sinh)))['tong'];
        $namSinh = (new ThanSoHoc())->tinhTong(date("Y", strtotime($user->ngay_sinh)))['tong'];
        $mocCuocDoi = [1 => null, 2 => null, 3 => null, 4 => null];
        $mocCuocDoi[1] = [
            'tuoi' => 36 - $chiSoNgaySinh,
            'con_so' => (new ThanSoHoc())->tinhTong($chiSoNgaySinh + $thangSinh)['tong']
        ];
        $mocCuocDoi[2] = [
            'tuoi' => $mocCuocDoi[1]['tuoi'] + 9,
            'con_so' => (new ThanSoHoc())->tinhTong($chiSoNgaySinh + $namSinh)['tong']
        ];
        if(!in_array(intval($mocCuocDoi[1]['con_so'] + $mocCuocDoi[2]['con_so']), [10, 11, 22, 33]))
            $con_so = (new ThanSoHoc())->tinhTong($mocCuocDoi[1]['con_so'] + $mocCuocDoi[2]['con_so'])['tong'];
        else
            $con_so = $mocCuocDoi[1]['con_so'] + $mocCuocDoi[2]['con_so'];
        $mocCuocDoi[3] = [
            'tuoi' => $mocCuocDoi[2]['tuoi'] + 9,
            'con_so' => $con_so
        ];
        if(!in_array(intval($thangSinh + $namSinh), [10, 11, 22, 33]))
            $con_so = (new ThanSoHoc())->tinhTong($thangSinh + $namSinh)['tong'];
        else
            $con_so = $thangSinh + $namSinh;
        $mocCuocDoi[4] = [
            'tuoi' => $mocCuocDoi[3]['tuoi'] + 9,
            'con_so' => $con_so
        ];
        $str = [];
        foreach ($mocCuocDoi as $index => $item) {
            $str[] = 'Mốc '.$index.': Tuổi '.$item['tuoi'].' số '.$item['con_so'];
        }
        return implode('<br/>', $str);
    }
    public static function getNoiDungDuongDoi($so){
        return Cauhinh::findOne(['ghi_chu' => 'duong_doi_so_'.$so])->content;
    }
    /**
     * @param $so
     * @param $nhom
     * @return array|YNghiaCacConSo[]|\yii\db\ActiveRecord[]
     */
    public static function getLuanGiai($so, $nhom){
        if(!is_array($so))
            $yNghiaCacConSo = YNghiaCacConSo::findAll(['chi_so' => $so, 'nhom' => $nhom]);
        else{
            $yNghiaCacConSo = [];
            foreach ($so as $item) {
                $yNghiaCacConSo[] = YNghiaCacConSo::find()->andFilterWhere(['chi_so' => $item, 'nhom' => $nhom])->one();
            }
        }
        if(!is_null($yNghiaCacConSo))
            return $yNghiaCacConSo;
        return  [];
    }
    /**
     * @return string
     */
    public static function getEmailGui(){
        return Cauhinh::findOne(['ghi_chu' => 'email_gui_thong_tin'])->content;
    }
    /**
     * @return false|string[]
     */
    public static function getEmailNhanMaGiaoDich(){
        return explode(',', str_replace(' ', '', Cauhinh::findOne(['ghi_chu' => 'email_nhan_ma_giao_dich'])->content));
    }
    /**
     * @return false|string[]
     */
    public static function getEmailNhanThongBaoHangNgay(){
        return explode(',',Cauhinh::findOne(['ghi_chu' => 'email_nhan_thong_bao_hang_ngay'])->content);
    }
    /**
     * @param $user User
     * @param $soTien int
     * @return int
     */
    public static function tinhSoLuotXem($user, $soTien){
        if($user->VIP == 1)
            $tile = Cauhinh::findOne(['ghi_chu' => 'ti_le_chi_phi_vip'])->content;
        else
            $tile = Cauhinh::findOne(['ghi_chu' => 'ti_le_chi_phi'])->content;
        $arr_tile = explode('-', $tile);
        $soLuotXem = round($soTien/1000) * intval(trim($arr_tile[1])) / intval(trim($arr_tile[0]));
        return  $soLuotXem;
    }
    /**
     * @param $user User
     */
    public static function tinhChiSoNgaySinh($user){
        $tong = intval(date("d", strtotime($user->ngay_sinh))) ;
        return (new ThanSoHoc())->tinhTong($tong)['tong'];
    }
    /**
     * @param $user User
     */
    public static function tinhChiSoVongDoi($user){
        return [
            'VD1' => (new ThanSoHoc())->tinhTong(date('n', strtotime($user->ngay_sinh)))['tong'],
            'VD2' => (new ThanSoHoc())->tinhTong(date('j', strtotime($user->ngay_sinh)))['tong'],
            'VD3' => (new ThanSoHoc())->tinhTong(date('Y', strtotime($user->ngay_sinh)))['tong'],
        ];
    }
    /**
     * @param $user User
     */
    public static function tinhNamHienTai($user){
        $strNgay = strval(date("d", strtotime($user->ngay_sinh)));
        $strThang = strval(date("m", strtotime($user->ngay_sinh)));
        $tong = (new ThanSoHoc())->tinhTong(intval($strNgay.$strThang))['tong'];
        return (new ThanSoHoc())->tinhTong($tong + (new ThanSoHoc())->tinhTong(date("Y")))['tong'];
    }
    /**
     * @param $user User
     */
    public static function tinhDauAnCuocDoi($user){
        $dauAnCuocDoi = [];
        $duongDoi = self::tinhDuongDoi($user)['duong_doi'];
        $dauAnCuocDoi[] = 36 - $duongDoi;
        for($i = 1; $i<=7; $i++)
            $dauAnCuocDoi[] = $dauAnCuocDoi[$i - 1] + 9;
        return implode(', ', $dauAnCuocDoi);
    }
    /**
     * @param $namSinh int
     */
    public static function getCanChi($namSinh){
        $can = ['Canh', 'Tân', 'Nhâm', 'Quý', "Giáp", "Ất", "Bính", "Đinh", "Mậu", "Kỷ"];
        $chi = ['Tý', 'Sửu', 'Dần', 'Mão', 'Thìn', 'Tỵ', 'Ngọ', 'Mùi', 'Thân', 'Dậu', 'Tuất', 'Hợi'];
        $strNamSinh = strval($namSinh);
        $hangChi = intval($strNamSinh[2].$strNamSinh[3]);
        if($namSinh < 2000){
            $phanDu = $hangChi % 12;
        }else{
            $phanDu = ($namSinh - 1960) % 12;
        }
        return [
            'can' => $can[intval($strNamSinh[3])],
            'chi' => $chi[$phanDu]
        ];
    }
    public static function getMenh($namSinh){
        $canChi = self::getCanChi($namSinh);
        $hangCan = [
            'Giáp' => 1, 'Ất' => 1,
            'Bính' => 2, 'Đinh' => 2,
            'Mậu' => 3, 'Kỷ' => 3,
            'Canh' => 4, 'Tân' => 4,
            'Nhâm' => 5, 'Quý' => 5
        ];
        $hangChi = [
            'Tý' => 0, 'Sửu' => 0, 'Ngọ' => 0, 'Mùi' => 0,
            'Dần' => 1, 'Mão' => 1, 'Thân' => 1, 'Dậu' => 1,
            'Thìn' => 2, 'Tỵ' => 2, 'Tuất' => 2, 'Hợi' => 2
        ];
        $banMenh = [
            1 => 'Kim',
            2 => 'Thủy',
            3 => 'Hỏa',
            4 => 'Thổ',
            5 => 'Mộc'
        ];
        $Menh = intval($hangCan[$canChi['can']] + $hangChi[$canChi['chi']]);
        while($Menh > 5)
            $Menh -= 5;
        return $banMenh[$Menh];
    }
    /**
     * @param $user User
     * @param $type string //<p>Tương khắc, Tương sinh</p>
     */
    public static function getMauSac($user){
        $mauSacTuongSinh = [
            'Kim' => ['ten_mau' => 'Vàng, Nâu đất', 'ma_mau' => '#6a3300, #ffff01'],
            'Mộc' => ['ten_mau' => 'Xanh nước, Đen', 'ma_mau' => '#2687be, #000000'],
            'Thủy' => ['ten_mau' => 'Trắng, Xám', 'ma_mau' => '#ffffff, #b0b0b0'],
            'Hỏa' => ['ten_mau' => 'Xanh lục', 'ma_mau' => '#4dc049'],
            'Thổ' => ['ten_mau' => 'Đỏ, Hồng, Tím', 'ma_mau' => '#e61f24, #ff007e, #ff00fc']
        ];
        $mauSacTuongKhac = [
            'Kim' => ['ten_mau' => 'Đỏ, Hồng, Tím', 'ma_mau' => '#e61f24, #ff007e, #ff00fc'],
            'Mộc' => ['ten_mau' => 'Trắng, Xám', 'ma_mau' => '#ffffff, #b0b0b0'],
            'Thủy' => ['ten_mau' => 'Vàng, Nâu đất', 'ma_mau' => '#6a3300, #ffff01'],
            'Hỏa' => ['ten_mau' => 'Xanh nước, Đen', 'ma_mau' => '#2687be, #000000'],
            'Thổ' => ['ten_mau' => 'Xanh lục', 'ma_mau' => '#4dc049']
        ];
        $menh = self::getMenh(date("Y", strtotime($user->ngay_sinh)));
        $mauTuongSinh = implode('<br />', [$mauSacTuongSinh[$menh]['ten_mau'], $mauSacTuongSinh[$menh]['ma_mau']]);
        $mauTuongKhac = implode('<br />', [$mauSacTuongKhac[$menh]['ten_mau'], $mauSacTuongKhac[$menh]['ma_mau']]);
        return [
            'mau_tuong_sinh' => $mauTuongSinh,
            'mau_tuong_khac' => $mauTuongKhac
        ];
    }
    public static function getGioSinh(){
        $content = Cauhinh::findOne(['ghi_chu' => 'khoang_gio_sinh'])->content;
        $khoang_gio = explode('<br />', nl2br($content));
        $options = [];
        foreach ($khoang_gio as $item) {
            $options[trim($item)] = trim($item);
        }
        return $options;
    }
    /**
     * @param $user User
     */
    public static function tinhDinhTruongThanh($user, $soDuongDoi){
        $ngay_sinh = (new ThanSoHoc())->tinhTong(date("j", strtotime($user->ngay_sinh)))['tong'];
        if($ngay_sinh == 11) $ngay_sinh = 2;
        else if($ngay_sinh == 22) $ngay_sinh = 4;
        $thang = (new ThanSoHoc())->tinhTong(date("n", strtotime($user->ngay_sinh)))['tong'];
        if($thang == 11) $thang = 2;
        $dinh1 = (new ThanSoHoc())->tinhTong($ngay_sinh + $thang)['tong'];
        if($dinh1 == 11) $dinh1 = 2;
        else if($dinh1 == 22) $dinh1 = 4;
        else if($dinh1 == 33) $dinh1 = 6;
        $nam_sinhGoc = (new ThanSoHoc())->tinhTong(date("Y", strtotime($user->ngay_sinh)))['tong'];
        if($nam_sinhGoc == 11) $nam_sinh = 2;
        else if($nam_sinhGoc == 22) $nam_sinh = 4;
        else if($nam_sinhGoc == 33) $nam_sinh = 6;
        else $nam_sinh = $nam_sinhGoc;
        $dinh2 = (new ThanSoHoc())->tinhTong($ngay_sinh + $nam_sinh)['tong'];
        if($dinh2 == 11) $dinh2 = 2;
        else if($dinh2 == 22) $dinh2 = 4;
        else if($dinh2 == 33) $dinh2 = 6;
        $dinh3 = (new ThanSoHoc())->tinhTong($dinh1 + $dinh2)['tong'];
        if($dinh3 == 33)
            $dinh3 = 6;
        $dinh4 = (new ThanSoHoc())->tinhTong($thang + $nam_sinhGoc)['tong'];
        if($dinh4 == 33)
            $dinh4 = 6;

        if($soDuongDoi == 22)
            $soDuongDoi = 4;
        else if($soDuongDoi == 33)
            $soDuongDoi = 6;

        $tuoiDinh1 = 36 - $soDuongDoi;
        $tuoiDinh2 = $tuoiDinh1 + 9;
        $tuoiDinh3 = $tuoiDinh2 + 9;
        $tuoiDinh4 = $tuoiDinh3 + 9;
        return [
            'dinh' => 'ĐỈNH 1: '.$dinh1.' - ĐỈNH 2: '.$dinh2.' - ĐỈNH 3: '.$dinh3.' - ĐỈNH 4: '.$dinh4,
            'tuoiDinh' => 'ĐỈNH 1: '.$tuoiDinh1.' - ĐỈNH 2: '.$tuoiDinh2.' - ĐỈNH 3: '.$tuoiDinh3.' - ĐỈNH 4: '.$tuoiDinh4,
            'arr_dinh' => [$dinh1, $dinh2, $dinh3, $dinh4]
        ];
    }
    /**
     * @param $user User
     */
    public static function tinhSoThuThach($user){
        $ngay_sinh = (new ThanSoHoc())->tinhTong(date("j", strtotime($user->ngay_sinh)))['tong'];
        if($ngay_sinh == 11) $ngay_sinh = 2;
        else if($ngay_sinh == 22) $ngay_sinh = 4;
        $thang = (new ThanSoHoc())->tinhTong(date("n", strtotime($user->ngay_sinh)))['tong'];
        if($thang == 11) $thang = 2;
        $nam_sinhGoc = (new ThanSoHoc())->tinhTong(date("Y", strtotime($user->ngay_sinh)))['tong'];
        if($nam_sinhGoc == 11) $nam_sinh = 2;
        else if($nam_sinhGoc == 22) $nam_sinh = 4;
        else if($nam_sinhGoc == 33) $nam_sinh = 6;
        else $nam_sinh = $nam_sinhGoc;
        $thuThach1 = abs($ngay_sinh - $thang);
        $thuThach2 = abs($ngay_sinh - $nam_sinh);
        $thuThach3 = abs($thuThach1 - $thuThach2);
        $thuThach4 = abs($thang - $nam_sinh);
        return [
            'thuThach' => "THỬ THÁCH 1: {$thuThach1} - THỬ THÁCH 2: {$thuThach2} - THỬ THÁCH 3: {$thuThach3} - THỬ THÁCH 4: {$thuThach4}",
            'thoiGianThuThach' => '<span class="col_thu_thach_1">THỬ THÁCH 1: Từ 0 tuổi đến 30-35</span>
<span class="col_thu_thach_2">THỬ THÁCH 2: Từ 30-35 đến 55-60</span><br/>
<span class="col_thu_thach_3">THỬ THÁCH 3: Từ 0 đến Tạm biệt</span>
<span class="col_thu_thach_4">THỬ THÁCH 4: >55-60</span>',
            'arr_so' => [$thuThach1, $thuThach2, $thuThach3, $thuThach4]
        ];
    }
    /**
     * @param $user User
     */
    public static function tinhSoLyTri($user){
        $arr_str = [
            'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5, 'f' => 6, 'g' => 7, 'h' => 8, 'i' => 9,
            'j' => 1, 'k' => 2, 'l' => 3, 'm' => 4, 'n' => 5, 'o' => 6, 'p' => 7, 'q' => 8, 'r' => 9,
            's' => 1, 't' => 2, 'u' => 3, 'v' => 4, 'w' => 5, 'x' => 6, 'y' => 7, 'z' => 8
        ];
        $codeHoTen = myAPI::createCode($user->hoten);
        $arr_ho_ten = explode('-', $codeHoTen);
        $ten = $arr_ho_ten[count($arr_ho_ten) - 1];
        $ngaySinh = date("j", strtotime($user->ngay_sinh));
        $tongTen = 0;
        for ($i=0; $i<strlen($ten); $i++){
            if(isset($arr_str[$ten[$i]]))
                $tongTen += $arr_str[$ten[$i]];
        }
        $tongTen = (new ThanSoHoc())->tinhTong($tongTen)['tong'];
        if($tongTen == 33)
            $tongTen = 6;
        $ngaySinh = (new ThanSoHoc())->tinhTong($ngaySinh)['tong'];
        if($ngaySinh == 33)
            $ngaySinh = 6;
        $tong = (new ThanSoHoc())->tinhTong($tongTen + $ngaySinh)['tong'];
        if($tong == 33)
            return  6;
        return $tong;
    }
    /**
     * @param $user User
     * @param null|integer $namTinh
     * @return int|mixed
     */
    public static function tinhNamThanSo($user, $namTinh = null){
        if(is_null($namTinh)){
            $namTheGioi = Cauhinh::findOne(['ghi_chu' => 'nam_than_so'])->content;
        }
//            $namTheGioi = (new ThanSoHoc())->tinhTong(date("Y"))['tong'];
        else
            $namTheGioi = (new ThanSoHoc())->tinhTong($namTinh)['tong'];
        if($namTheGioi == 11)
            $namTheGioi = 2;
        else if($namTheGioi == 22)
            $namTheGioi = 4;
        else if($namTheGioi == 33)
            $namTheGioi = 6;

        $ngaySinh = (new ThanSoHoc())->tinhTong(date("j", strtotime($user->ngay_sinh)))['tong'];
        if($ngaySinh == 11)
            $ngaySinh = 2;
        else if($ngaySinh == 22)
            $ngaySinh = 4;

        $thangSinh = (new ThanSoHoc())->tinhTong(date("m", strtotime($user->ngay_sinh)))['tong'];
        if($thangSinh == 11)
            $thangSinh = 2;
        else if($thangSinh == 22)
            $thangSinh = 4;

        $tong = (new ThanSoHoc())->tinhTong($ngaySinh + $namTheGioi + $thangSinh)['tong'];
        if($tong == 11)
            return 2;
        if($tong == 22)
            return 4;
        if($tong == 33)
            return 6;
        return $tong;
    }
    /**
     * @param $namThanSo int
     */
    public static function tinhThangThanSo($namThanSo, $thang = null){
        if(is_null($thang))
            $thangDuongLich = date("m");
        else
            $thangDuongLich = $thang;
        if($thangDuongLich == 11)
            $thangDuongLich = 2;
        else if($thangDuongLich == 12)
            $thangDuongLich = 3;
        else if($thangDuongLich == 10)
            $thangDuongLich = 1;
        $tong = (new ThanSoHoc())->tinhTong($namThanSo + $thangDuongLich)['tong'];
        if($tong == 11)
            return 2;
        if($tong == 22)
            return 4;
        if($tong == 33)
            return 6;
        return $tong;
    }
    /**
     * @param $user User
     * @param $duongDoi int
     * @param $soNgaySinh int
     * @param $soVanMenh int
     * @param $soTruongThanh int
     * @param $soBanThe int
     * @param $soLyTri int
     * @param $soTamHon int
     */
    public static function bieuDoSoCaNhan($user, $duongDoi, $soVanMenh, $soTruongThanh, $soBanThe, $soLyTri, $soTamHon){
        $arr_str = [
            'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5, 'f' => 6, 'g' => 7, 'h' => 8, 'i' => 9,
            'j' => 1, 'k' => 2, 'l' => 3, 'm' => 4, 'n' => 5, 'o' => 6, 'p' => 7, 'q' => 8, 'r' => 9,
            's' => 1, 't' => 2, 'u' => 3, 'v' => 4, 'w' => 5, 'x' => 6, 'y' => 7, 'z' => 8
        ];
        $arrInt = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0];
        $arrRepeatCharacter = [];
        $arrRepeatNumber = [];
        $arrCacLoaiConSo = [];
        foreach ($arr_str as $char => $item) {
            $arrRepeatCharacter[$char] = 0;
        }
        foreach ($arrInt as $number => $item) {
            $arrRepeatNumber[$number] = 0;
        }

        $code_ho_ten = mb_strtolower(str_replace('-', '', trim(myAPI::createCode($user->hoten))));
        $ngaySinh = date("j/n/Y", strtotime($user->ngay_sinh));
        $arrNgaySinh = explode('/', $ngaySinh);

        if(strpos($arrNgaySinh[2], '11') !== false){
            $arrInt[2]++;
        }
        else if(strpos($arrNgaySinh[2], '22') !== false){
            $arrInt[4]++;
        }
        else if(strpos($arrNgaySinh[2], '33') !== false){
            $arrInt[6]++;
        }
        for($i = 0; $i<strlen(strval($arrNgaySinh[2])); $i++)
            $arrInt[$arrNgaySinh[2][$i]]++;

        for ($i=0; $i<strlen($code_ho_ten); $i++){
            if(isset($code_ho_ten[$i])){
                if(isset($arr_str[$code_ho_ten[$i]])){
                    $arrInt[$arr_str[$code_ho_ten[$i]]]++;
                    $arrRepeatCharacter[$code_ho_ten[$i]]++;
                }
            }
        }


        $arrCacConSoKhac = [
            ['type' => 'Ngày sinh', 'value' => $arrNgaySinh[0]],
            ['type' => 'Tháng sinh', 'value' => $arrNgaySinh[1]],
            ['type' => 'Đường đời', 'value' => $duongDoi],
            ['type' => 'Số vận mệnh', 'value' => $soVanMenh],
//            ['type' => 'Số bản thể', 'value' => $soBanThe],
//            ['type' => 'Số lý trí', 'value' => $soLyTri],
            ['type' => 'Số tâm hồn', 'value' => $soTamHon],
            ['type' => 'Số trưởng thành', 'value' => $soTruongThanh],
        ];
        foreach ($arrCacConSoKhac as $item) {
            if($item['value'] == 11){
                $arrInt[2]++;
                $arrInt[1]+=2;
            }
            else if($item['value'] == 22){
                $arrInt[2]+=2;
                $arrInt[4]++;
            }
            else if($item['value'] == 33){
                $arrInt[3]+=2;
                $arrInt[6]++;
            }
            else {
                for($indexStr = 0; $indexStr < strlen(strval($item['value'])); $indexStr++){
                    $arrInt[intval(strval($item['value'])[$indexStr])]++;
                }
            };

            $arrCacLoaiConSo[$item['type']] = $item['value'];
        }

//        VarDumper::dump($arrCacLoaiConSo, 10, true); exit;
        $tong = 0;
        foreach ($arrInt as $i)
            $tong += $i;

        $arrTiLe = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0];
        foreach ($arrTiLe as $index => $i){
            $arrTiLe[$index] = [
                'con_so' => $index,
                'ti_le' => round($arrInt[$index] * 100/$tong, 2)
            ];
        }

        $arrNoiDung = [
            0 => 'Tiềm năng vô tận',
            1 => 'Có định hướng kiên định, cá tính, độc lập, mạnh mẽ, lãnh đạo, năng nổ',
            2 => 'Hợp tác, nhẹ nhàng, ngoại giao, hoà bình, nhạy cảm, trực giác, kiên cường, duyên dáng, tinh tế, không đối đầu',
            3 => 'Sáng tạo, giỏi ngôn từ, dí dỏm, truyền cảm hứng, lạc quan, tính tự thể hiện cao, thiếu trách nhiệm, thích làm nhiều thứ - thiếu tập trung',
            4 => 'Đáng tin cậy, đúng giờ, hướng vào chi tiết, kiên nhẫn, óc quản lý tốt, tổ chức, tham công tiếc việc.',
            5 => 'Yêu tự do, năng động, bốc đồng, suy nghĩ nhanh, dễ chán, dễ thích nghi, linh hoạt, không thông thường, thích phiêu lưu, can đảm, thích mạo hiểm, tỉnh thức tâm linh',
            6 => 'Hài hoà, yêu thương, chăm sóc, hy sinh bản thân, hướng về gia đình, luôn hướng dẫn người khác, người chữa bệnh, an ủi',
            7 => 'Trí tuệ, tâm linh, hướng nội, hiếu học, nghiêm túc, thích ở một mình, ít nói, có thể hoài nghi. Người tìm kiếm, người tìm kiếm, triết gia',
            8 => 'Có định hướng và tham vọng, có thẩm quyền, độc lập, quyền lực và tiền bạc, định hướng kinh doanh, nhìn xa trông rộng, lạc quan, tham lam',
            9 => 'Nhân đạo, hy sinh, ý thức giúp đỡ lớn (tầm vỹ mô), cho đi, tinh tế, chính trị, hài phóng, kiêu ngạo'
        ];
        // Sắp xếp
        for($i = 1; $i<9; $i++){
            for($j = $i+ 1; $j<=9; $j++){
                if($arrTiLe[$i]['ti_le'] < $arrTiLe[$j]['ti_le']){
                    $a = $arrTiLe[$i];
                    $arrTiLe[$i] = $arrTiLe[$j];
                    $arrTiLe[$j] = $a;
                }
            }
        }
        $tbody = [];
        foreach ($arrTiLe as $index => $item) {
            if($index > 0){
                $tbody[] = '<tr><td style="text-align: center; font-size: 12pt">'.sprintf('%02d', $item['con_so']).'</td><td style="text-align: right; font-size: 12pt">'.$item['ti_le'].'%</td><td>'.$arrNoiDung[$item['con_so']].'</td></tr>';
            }
        }
        // Sinh Bảng
        $table = '<table style="border-collapse: collapse; width: 100%;" border="1">
            <thead>
                <tr style="background-color: #cb5205"><th width="10%">Số</th><th width="8%">Tỉ lệ</th><th>Ý nghĩa</th></tr>
            </thead>
            <tbody>'.implode('', $tbody).'</tbody>
        </table>';
        return $table;
    }
    /**
     * @param $user
     * @return string|string[]
     */
    public static function bieuDoNgaySinh($user){
        $ngaySinh = str_replace('-','', $user->ngay_sinh);
        $arrInt = [1 => '', 2 => '', 3 => '', 4 => '', 5 => '', 6 => '', 7 => '', 8 => '', 9 => '', 0 => ''];
        for($i = 0; $i < strlen($ngaySinh); $i++){
            $arrInt[$ngaySinh[$i]] .= $ngaySinh[$i];
        }
//        $ketQuaDaySo = [];
//        $ketQuaText = [];
        $table = "<table class='table table-bordered table-striped'>
<tr><td width='33%' style='text-align: center; height: 50px'>{{3}}</td><td width='33%' style='text-align: center; height: 50px'>{{6}}</td><td width='33%' style='text-align: center; height: 50px'>{{9}}</td></tr>
<tr><td width='33%' style='text-align: center; height: 50px'>{{2}}</td><td width='33%' style='text-align: center; height: 50px'>{{5}}</td><td width='33%' style='text-align: center; height: 50px'>{{8}}</td></tr>
<tr><td width='33%' style='text-align: center; height: 50px'>{{1}}</td><td width='33%' style='text-align: center; height: 50px'>{{4}}</td><td width='33%' style='text-align: center; height: 50px'>{{7}}</td></tr>
</table>";
        for($i = 1; $i<=9; $i++){
            if($arrInt[$i] != '')  $str = $arrInt[$i]; else $str = '';
            $table = str_replace('{{'.$i.'}}', $str, $table);
        }
//        $ketQuaDaySoTrong = [];
//        $ketQuaTextTrong = [];
//        if($arrInt[1] > 0 && $arrInt[4] > 0 && $arrInt[7] > 0){ $ketQuaDaySo[] = '1-4-7'; $ketQuaText[] = 'Mũi tên thực tế'; }
//        if($arrInt[1] > 0 && $arrInt[2] > 0 && $arrInt[3] > 0){ $ketQuaDaySo[] = '1-2-3'; $ketQuaText[] = 'Mũi tên Tổ chức'; }
//        if($arrInt[2] > 0 && $arrInt[5] > 0 && $arrInt[8] > 0){ $ketQuaDaySo[] = '2-5-8'; $ketQuaText[] = 'Cân bằng cảm xúc'; }
//        if($arrInt[3] > 0 && $arrInt[6] > 0 && $arrInt[9] > 0){ $ketQuaDaySo[] = '3-6-9'; $ketQuaText[] = 'Trí tuệ'; }
//        if($arrInt[4] > 0 && $arrInt[5] > 0 && $arrInt[6] > 0){ $ketQuaDaySo[] = '4-5-6'; $ketQuaText[] = 'Ý chí'; }
//        if($arrInt[7] > 0 && $arrInt[8] > 0 && $arrInt[9] > 0){ $ketQuaDaySo[] = '7-8-9'; $ketQuaText[] = 'Hành động'; }
//        if($arrInt[1] > 0 && $arrInt[5] > 0 && $arrInt[9] > 0){ $ketQuaDaySo[] = '1-5-9'; $ketQuaText[] = 'Kiên định'; }
//        if($arrInt[3] > 0 && $arrInt[5] > 0 && $arrInt[7] > 0){ $ketQuaDaySo[] = '3-5-7'; $ketQuaText[] = 'Kiên định'; }
//
//        if($arrInt[1] == 0 && $arrInt[4] == 0 && $arrInt[7] == 0){ $ketQuaDaySoTrong[] = '1-4-7'; $ketQuaTextTrong[] = '......'; }
//        if($arrInt[1] == 0 && $arrInt[2] == 0 && $arrInt[3] == 0){ $ketQuaDaySoTrong[] = '1-2-3'; $ketQuaTextTrong[] = '......'; }
//        if($arrInt[2] == 0 && $arrInt[5] == 0 && $arrInt[8] == 0){ $ketQuaDaySoTrong[] = '2-5-8'; $ketQuaTextTrong[] = 'Nhạy cảm'; }
//        if($arrInt[3] == 0 && $arrInt[6] == 0 && $arrInt[9] == 0){ $ketQuaDaySoTrong[] = '3-6-9'; $ketQuaTextTrong[] = 'Trí nhớ ngắn hạn'; }
//        if($arrInt[4] == 0 && $arrInt[5] == 0 && $arrInt[6] == 0){ $ketQuaDaySoTrong[] = '4-5-6'; $ketQuaTextTrong[] = '......'; }
//        if($arrInt[7] == 0 && $arrInt[8] == 0 && $arrInt[9] == 0){ $ketQuaDaySoTrong[] = '7-8-9'; $ketQuaTextTrong[] = '......'; }
//        if($arrInt[1] == 0 && $arrInt[5] == 0 && $arrInt[9] == 0){ $ketQuaDaySoTrong[] = '1-5-9'; $ketQuaTextTrong[] = 'Trì trệ'; }
//        if($arrInt[3] == 0 && $arrInt[5] == 0 && $arrInt[7] == 0){ $ketQuaDaySoTrong[] = '3-5-7'; $ketQuaTextTrong[] = 'Hoài nghi'; }
//
//        $strKetQua = '<div class="row"><div class="col-md-6">'.$table.'</div><div class="col-md-6"><p><strong>Kết quả: </strong><br/>'.implode('<br/>', $ketQuaText).'</p><p><strong>Số thiếu: </strong>'.implode('<br/>', $ketQuaTextTrong).'</p></div></div>';
        return $table;
    }
    /**
     * @param $duongDoi integer
     * @param $vanMenh int
     * @return int
     */
    public static function tinhSoTruongThanh($duongDoi, $vanMenh){
        $tong = (new ThanSoHoc())->tinhTong($duongDoi + $vanMenh)['tong'];
        return $tong;
    }
    /**
     * @param $dai_ly_id int
     * @param $thang int
     * @param $nam int
     * @return int|mixed
     * @throws \yii\db\Exception
     */
    public static function getDoanhSo($dai_ly_id, $thang, $nam){
        // Tính lại doanh số của đại lý trong tháng
        $result2 = \Yii::$app->db->createCommand("SELECT sum(so_tien_can_thanh_toan) AS doanh_so 
FROM rv_user 
WHERE dai_ly_id = :d and date_format(ngay_thanh_toan_lan_dau, '%m') = :m and date_format(ngay_thanh_toan_lan_dau, '%Y') = :y", [
            ':d' => $dai_ly_id,
            ':m' => $thang,
            ':y' => $nam
        ])->queryAll();
        return count($result2) > 0 ? $result2[0]['doanh_so'] : 0;
    }
    /**
     * @param $dai_ly_id int
     * @param $thang int
     * @param $nam int
     * @return int|mixed
     * @throws \yii\db\Exception
     */
    public static function getThongKeDoanhSoDaiLy($dai_ly_id, $thang, $nam){
        // Tính lại doanh số của đại lý trong tháng
        $result2 = \Yii::$app->db->createCommand("SELECT sum(so_tien_can_thanh_toan) AS doanh_so, count(id) as so_bai, sum(so_tien)/count(id) as so_tien, sum(khuyen_mai) as khuyen_mai, sum(loi_nhuan) as loi_nhuan
FROM rv_user 
WHERE dai_ly_id = :d and date_format(ngay_thanh_toan_lan_dau, '%m') = :m and date_format(ngay_thanh_toan_lan_dau, '%Y') = :y", [
            ':d' => $dai_ly_id,
            ':m' => $thang,
            ':y' => $nam
        ])->queryAll();
        return count($result2) > 0 ? [
            'doanh_so' => $result2[0]['doanh_so'],
            'so_bai' => $result2[0]['so_bai'],
            'so_tien' => $result2[0]['so_tien'],
            'khuyen_mai' => $result2[0]['khuyen_mai'],
            'loi_nhuan' => $result2[0]['loi_nhuan'],
        ] : [
            'doanh_so' => 0,
            'so_bai' => 0,
            'so_tien' => 0,
            'khuyen_mai' => 0,
            'loi_nhuan' => 0
        ];
    }
    /**
     * @param $dai_ly_id int
     * @param $thang int
     * @param $nam int
     * @return int|mixed
     * @throws \yii\db\Exception
     */
    public static function getTienDauTu($dai_ly_id, $thang, $nam){
        // Tính tổng tiền đầu tư của đại lý
        $result = \Yii::$app->db->createCommand("SELECT sum(so_tien_giao_dich) AS tien_dau_tu FROM rv_giao_dich WHERE khach_hang_id = :d and date_format(ngay_giao_dich, '%m') = :m and date_format(ngay_giao_dich, '%Y') = :y and trang_thai_giao_dich = :t and mua_goi = 1", [
            ':d' => $dai_ly_id,
            ':m' => $thang,
            ':y' => $nam,
            ':t' => 'Thành công'
        ])->queryAll();
        return count($result) > 0 ? $result[0]['tien_dau_tu'] : 0;
    }
    /**
     * @param $dai_ly_id int
     * @param $thang int
     * @param $nam int
     * @return array|int[]
     * @throws \yii\db\Exception
     */
    public static function getThongKeDauTu($dai_ly_id, $thang, $nam){
        // Tính tổng tiền đầu tư của đại lý
        $result = \Yii::$app->db->createCommand("SELECT sum(so_luot_xem) as so_bai, sum(don_gia * so_luot_xem)/sum(so_luot_xem) as gia_von, sum(so_tien_giao_dich) AS tien_dau_tu, sum(so_tien) as so_tien, sum(khuyen_mai) as khuyen_mai
FROM rv_giao_dich WHERE khach_hang_id = :d and date_format(ngay_giao_dich, '%m') = :m and date_format(ngay_giao_dich, '%Y') = :y and trang_thai_giao_dich = :t and mua_goi = 1", [
            ':d' => $dai_ly_id,
            ':m' => $thang,
            ':y' => $nam,
            ':t' => 'Thành công'
        ])->queryAll();
        return count($result) > 0 ? [
            'tien_dau_tu' => $result[0]['tien_dau_tu'],
            'so_tien' => $result[0]['so_tien'],
            'so_bai' => $result[0]['so_bai'],
            'gia_von' => $result[0]['gia_von'],
            'khuyen_mai' => $result[0]['khuyen_mai'],
        ] : [
            'tien_dau_tu' => 0,
            'so_tien' => 0,
            'so_bai' => 0,
            'khuyen_mai' => 0,
        ];
    }

    public static function getThongKeBai($dai_ly_id, $thang, $nam){
// Tính tổng tiền đầu tư của đại lý
        $result = \Yii::$app->db->createCommand("
SELECT sum(so_luot_xem) as so_bai, loai_giao_dich, user_id, khach_hang_id
FROM rv_giao_dich
WHERE ( khach_hang_id = :u or user_id = :u)
  and date_format(ngay_giao_dich, '%m') = :m
  and date_format(ngay_giao_dich, '%Y') = :y
group by loai_giao_dich, user_id, khach_hang_id", [
            ':u' => $dai_ly_id,
            ':m' => $thang,
            ':y' => $nam,
        ])->queryAll();
//        VarDumper::dump(count($result), 10, true); exit;
        $ketQua = [];

        if(count($result) > 0){
            foreach ($result as $item) {
                if($item['loai_giao_dich'] == GiaoDich::MUA_BAI){
                    if($item['user_id'] == $dai_ly_id)
                        $ketQua['ban_goi'] = $item['so_bai'];
                    else
                        $ketQua['mua_bai'] = $item['so_bai'];
                }
                else if($item['loai_giao_dich'] == GiaoDich::XEM_BAI)
                    $ketQua['xem_bai'] = $item['so_bai'];
            }
        }else
            $ketQua = [
                'mua_bai' => 0,
                'xem_bai' => 0,
                'ban_goi' => 0
            ];
        return $ketQua;
    }
    /**
     * @param $dai_ly_id int
     * @return int|mixed
     * @throws \yii\db\Exception
     */
    public static function getLoiNhuanDaiLy($dai_ly_id){
        // Tính lại doanh số của đại lý trong tháng
        $result2 = \Yii::$app->db->createCommand("SELECT sum(loi_nhuan) AS loi_nhuan 
FROM rv_user 
WHERE dai_ly_id = :d", [
            ':d' => $dai_ly_id,
        ])->queryAll();
        return count($result2) > 0 ? $result2[0]['loi_nhuan'] : 0;
    }
    /**
     * @param $dai_ly_id int
     * @return int|mixed
     * @throws \yii\db\Exception
     */
    public static function getDoanhSoDaiLy($dai_ly_id){
        $result = \Yii::$app->db->createCommand('SELECT sum(so_tien_can_thanh_toan) AS doanh_so FROM rv_user WHERE dai_ly_id = :d and (so_tien_da_thanh_toan is not null and so_tien_da_thanh_toan > 0)', [
            ':d' => $dai_ly_id
        ])->queryAll();
        return count($result) > 0 ? $result[0]['doanh_so'] : 0;
    }
    public static function getXemBai($khach_hang_id){
        $chiSo = ChiSo::find()
            ->andFilterWhere(['khach_hang_id' => $khach_hang_id])
            ->orderBy(['thu_tu' => SORT_ASC])
            ->all();
        $noi_dung_bai = [];
        /** @var ChiSo $item */
        foreach ($chiSo as $item) {
            $yNghiaConSo = YNghiaChiSo::findAll(['chi_so_id' => $item->id]);
            if(count($yNghiaConSo) == 1){
                $yNghia = $yNghiaConSo[0]->yNghia;
                $noi_dung_bai[] = [
                    'nhom' => $yNghia->nhom.' '.$yNghia->chi_so,
                    'noi_dung' => $yNghia->noi_dung,
                    'chu_de' => $yNghia->chu_de,
                    'thu_tu' => $item->thu_tu
                ];
            }else if(count($yNghiaConSo) > 1){
                $nhom = $yNghiaConSo[0]->yNghia->nhom;
                $str = [];
                $chuDe = [];
                foreach ($yNghiaConSo as $index => $itemYNghiaConSo) {
                    $yNghia = $itemYNghiaConSo->yNghia;
                    $str[] = '<strong>'.mb_strtoupper($nhom).' '.($index + 1).' - số '.$yNghia->chi_so.': '.$yNghia->chu_de.'</strong><br/>'.$yNghia->noi_dung;
                    $chuDe[] = $nhom.' '.($index + 1).': '.$yNghia->chi_so;
                }
                $noi_dung_bai[] = [
                    'nhom' => $nhom,
                    'noi_dung' => implode('<br />', $str),
                    'chu_de' => implode(', ', $chuDe),
                    'thu_tu' => $item->thu_tu
                ];
            }else{
                $noi_dung_bai[] = [
                    'nhom' => $item->loai_chi_so,
                    'noi_dung' => $item->noi_dung,
                    'chu_de' => '',
                    'thu_tu' => $item->thu_tu
                ];
            }
        }
        return $noi_dung_bai;
    }
    public static function getXemBaiPrintPDF($khach_hang_id){
        $chiSo = ChiSo::find()
            ->andFilterWhere(['khach_hang_id' => $khach_hang_id])
            ->orderBy(['thu_tu' => SORT_ASC])
            ->all();
        $noi_dung_bai = [];
        /** @var ChiSo $item */
        foreach ($chiSo as $item) {
            $yNghiaConSo = YNghiaChiSo::findAll(['chi_so_id' => $item->id]);
            if(count($yNghiaConSo) == 1){
                $yNghia = $yNghiaConSo[0]->yNghia;
                if($item->loai_chi_so == ChiSo::SO_DO_NGAY_SINH){
                    $noi_dung_bai['BIỂU ĐỒ NGÀY SINH'] = [
                        'nhom' => 'BIỂU ĐỒ NGÀY SINH',
                        'chi_so' => '',
                        'noi_dung' => $item->noi_dung,
                        'chu_de' => '',
                        'thu_tu' => $item->thu_tu
                    ];
                }else{
                    $noi_dung_bai[$item->loai_chi_so] = [
                        'nhom' => $yNghia->nhom.' '.$yNghia->chi_so,
                        'chi_so' => $item->noi_dung,
                        'noi_dung' => $yNghia->noi_dung,
                        'chu_de' => $yNghia->chu_de,
                        'thu_tu' => $item->thu_tu
                    ];
                }
            }else if(count($yNghiaConSo) > 1){
                $nhom = $yNghiaConSo[0]->yNghia->nhom;
                $str = [];
                $chuDe = [];
                foreach ($yNghiaConSo as $index => $itemYNghiaConSo) {
                    $yNghia = $itemYNghiaConSo->yNghia;
                    $str[] = '<strong>'.mb_strtoupper($nhom).' '.($index + 1).' - số '.$yNghia->chi_so.': '.$yNghia->chu_de.'</strong><br/>'.$yNghia->noi_dung;
                    $chuDe[] = $nhom.' '.($index + 1).': '.$yNghia->chi_so;
                }
                $noi_dung_bai[$item->loai_chi_so] = [
                    'nhom' => $nhom,
                    'chi_so' => $item->noi_dung,
                    'noi_dung' => implode('<br />', $str),
                    'chu_de' => implode(', ', $chuDe),
                    'thu_tu' => $item->thu_tu
                ];
            }else{
                $noi_dung_bai[$item->loai_chi_so] = [
                    'nhom' => $item->loai_chi_so,
                    'chi_so' => $item->noi_dung,
                    'noi_dung' => $item->noi_dung,
                    'chu_de' => '',
                    'thu_tu' => $item->thu_tu
                ];
            }
        }
        return $noi_dung_bai;
    }
    /**
     * @param $yNghia YNghiaCacConSo
     * @param $tieuDe string
     * @param boolean $coH3
     * @return string
     */
    public static function getDoanVan($yNghia, $tieuDe, $noiDungLuanGiai = '', $urlImage = 'images/new/'){
        if($noiDungLuanGiai == '')
            $noiDung = str_replace('{{new_line}}', '</p>{{new_line}}<p>', $yNghia->noi_dung);
        else
            $noiDung = str_replace('{{new_line}}', '</p>{{new_line}}<p>', $noiDungLuanGiai);

        $doanVan = explode('{{new_line}}', $noiDung);
        $i = 0;
        if($yNghia->ngat_trang == 1)
            $content = '
            <div style="page-break-after: always;">
                <div style="background-image: url('.$urlImage.$yNghia->ten_file_anh.'); background-size: 600mm; height: 297mm"></div>
            </div>
            ';
        else{
            $content = '
            <div style="page-break-after: always;">
                <div style="background-image: url('.$urlImage.$yNghia->ten_file_anh.'); background-size: 100%;height: 297mm; padding-left: 60px; padding-right: 60px; padding-top:'.$yNghia->cach_tren.'px">
                    <h3 style="text-transform: uppercase; color: #cb5205">'.$tieuDe.'</h3>'.$doanVan[0].'
                </div>
            </div>
            ';
            $i = 1;
        }

        for ($j = $i; $j<count($doanVan); $j++){
            $title = '';
            if($j==0){
                $title = '<h3 style="text-transform: uppercase; color: #cb5205">'.$tieuDe.'</h3>';
            }

            if(trim(strip_tags($doanVan[$j])) !== ''){
                $content .= '<div style="page-break-after: always">
                            <div style="background-image: url('.$urlImage.'bgtinhhoa.jpg); background-size: 100%; height: 297mm">
                                <div style=" padding-top: 80px; padding-left: 60px; padding-right: 60px">
                                    '.$title.'
                                    <div>
                                        '.$doanVan[$j].'
                                    </div>
                                </div>
                            </div>
                        </div>';
            }

        }
        return $content;
    }


    /**
     * @param $yNghia YNghiaCacConSo
     * @param $tieuDe string
     * @param boolean $coH3
     * @return string
     */
    public static function getDoanVanHuongNghiep($yNghia, $tieuDe, $urlImage = 'images/new/huong-nghiep/'){
        if(intval($yNghia->chi_so) > 10){
            $tong = 0;
            for($i=0; $i < strlen(strval($yNghia->chi_so)); $i++)
                $tong+= intval(strval($yNghia->chi_so)[$i]);
        }else
            $tong = intval($yNghia->chi_so);

        $noiDung = YNghiaCacConSo::findOne(['chi_so' => $tong, 'nhom' => 'Chỉ số hướng nghiệp']);
        if(!is_null($noiDung))
            $noiDung = $noiDung->noi_dung;
        else{
            $noiDung  = '';
        }
        $doanVan = explode('{{new_line}}', $noiDung);
        $i = 0;
        $content = '';
        for ($j = $i; $j<count($doanVan); $j++){
            $title = '';
            if($j==0){
                $title = '<h3 style="text-transform: uppercase; color: #cb5205">'.$tieuDe.'</h3>';
            }

            if(trim(strip_tags($doanVan[$j])) !== ''){
                $content .= '<div style="page-break-after: always">
                            <div style="background-image: url('.$urlImage.'bgtinhhoa.jpg); background-size: 100%; height: 297mm">
                                <div style=" padding-top: 80px; padding-left: 60px; padding-right: 60px">
                                    '.$title.'
                                    <div>
                                        '.$doanVan[$j].'
                                    </div>
                                </div>
                            </div>
                        </div>';
            }

        }
        return $content;
    }

    /**
     * @param $noiDung string
     * @param $tieuDe string
     * @param false $coH3
     * @return string
     */
    public static function getDoanVan2($noiDung, $tieuDe, $coH3 = false, $doDai1Trang = 1800){
        $noiDung = str_replace('{{new_line}}', '', $noiDung);
        $strLen = mb_strlen($noiDung);
        $doanVan = [];

        if($strLen < $doDai1Trang)
            $doanVan = [$noiDung];
        else{
            $soDoanVan = round($strLen/$doDai1Trang);

            if($soDoanVan * $doDai1Trang < $strLen)
                $soDoanVan++;

            $from = 0;
            $viTriCuoiDoanVan = 0;
            for($dong = 1; $dong <= $soDoanVan; $dong++){
                if($viTriCuoiDoanVan + $doDai1Trang < $strLen)
                    $viTriCuoiDoanVan = mb_strpos($noiDung, ".", $viTriCuoiDoanVan + $doDai1Trang);
                else
                    $viTriCuoiDoanVan = $strLen - 1;

                $doanVan[] = mb_substr($noiDung,$from, $viTriCuoiDoanVan - $from + 1);
                $from = $viTriCuoiDoanVan + 1;
            }
        }

        $content = '';
        foreach ($doanVan as $index => $item){
            $title = '';
            if(!$coH3){
                if($index == 0) $title = '<h3 style="text-transform: uppercase; color: #cb5205">'.$tieuDe.'</h3>';
            }
            $url = 'images/new/bgtinhhoa.jpg';
            if(trim(strip_tags($item))!=''){
                $content .= '<div style="page-break-after: always">
                            <div style="background-image: url('.$url.'); background-size: 100%; height: 297mm">
                                <div style=" padding-top: 80px; padding-left: 60px; padding-right: 60px">
                                    '.$title.'
                                    <div>
                                        '.$item.'
                                    </div>
                                </div>
                            </div>
                        </div>';
            }

        }
        return $content;
    }
    public static function getDoanVan3($noiDung, $tieuDe, $coH3 = false, $doDai1Trang = 1800){
        $noiDung = str_replace('{{new_line}}', '', $noiDung);
        $strLen = mb_strlen($noiDung);
        $doanVan = [];

        if($strLen < $doDai1Trang)
            $doanVan = [$noiDung];
        else{
            $soDoanVan = round($strLen/$doDai1Trang);
            $from = 0;
            $viTriCuoiDoanVan = 0;
            for($dong = 1; $dong <= $soDoanVan; $dong++){
                if($viTriCuoiDoanVan + $doDai1Trang < $strLen)
                    $viTriCuoiDoanVan = mb_strpos($noiDung, ".", $viTriCuoiDoanVan + $doDai1Trang);
                else
                    $viTriCuoiDoanVan = $strLen - 1;

                $doanVan[] = mb_substr($noiDung,$from, $viTriCuoiDoanVan - $from + 1);
                $from = $viTriCuoiDoanVan + 1;
            }
        }

        $content = '';
        foreach ($doanVan as $index => $item){
            $title = '';
            if(!$coH3){
                if($index == 0) $title = '<h3 style="text-transform: uppercase; color: #cb5205">'.$tieuDe.'</h3>';
            }
            $url = 'images/new/bgtinhhoa.jpg';
            $content .= '<div style="page-break-after: always">
                            <div style="background-image: url('.$url.'); background-size: 100%; height: 297mm">
                                <div style=" padding-top: 80px; padding-left: 60px; padding-right: 60px">
                                    '.$title.'
                                    <div>
                                        '.$item.'
                                    </div>
                                </div>
                            </div>
                        </div>';
        }
        return $content;
    }
}
